use std::io::Error as IOError;

use etherparse::ReadError;

/// A type alias for results returned by RSCTP.
pub type Result<T> = ::std::result::Result<T, Error>;

/// RSCTP catches all errors in this Enum.
#[derive(Debug)]
pub enum Error {
    /// This will be returned in case a calculation returned an unwanted under- or overflow.
    Overflow,
    /// Packet is not directed at us.
    BadDestination,
    /// Packet doesn't contain Ethernet header.
    NoLink, // TODO: maybe rename this to `NoEthernetHeader`?
    /// No transport header in the packet.
    NoTransport, // unused
    /// SCTP can be transmitted over UDP, which we do not support from the get-go.
    NotSupported, // unused
    /// Packet is not an ethernet packet.
    NotEthernet, // unused
    /// Received packet is not an IP packet.
    NotIP,
    /// Received packet is not interesting to us (not SCTP).
    NotInteresting, // TODO: maybe rename this to `NotSCTP`?
    /// This can be returned by OS operations like creating a data link.
    IO(IOError),
    /// Etherparse read error
    Read(ReadError),
}

impl From<IOError> for Error {
    fn from(e: IOError) -> Self {
        Error::IO(e)
    }
}

impl From<ReadError> for Error {
    fn from(e: ReadError) -> Self {
        Error::Read(e)
    }
}
