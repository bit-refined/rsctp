/// Get the necessary amount of padding for a given length.
///
/// According to RFC 4960 § 3.2 and § 3.2.1 the total length of chunks and parameters MUST be a
/// multiple of 4 bytes. If the length is not a multiple of 4 bytes, the sender MUST pad the data
/// with all zero bytes, and this padding is not included the the respective Length field. The
/// sender MUST NOT pad with more than 3 bytes. The receiver MUST ignore these padding bytes.
#[allow(clippy::integer_arithmetic)]
pub const fn length(length: u16) -> u16 {
    (4 - (length % 4)) % 4
}

/// Get the total length of some data including necessary padding.
#[allow(clippy::integer_arithmetic)]
pub const fn total_padded_length(length: u16) -> usize {
    length as usize + self::length(length) as usize
}

/// Write bytes to a `Vec<u8>` including a padding at the end to a multiple of 4 bytes. This
/// function calculates the necessary amount of padding, preallocates a `Vec<u8>` large enough to
/// store data and padding and calls `write_data` to write the data to that `Vec<u8>`. The padding
/// is then appended and the full `Vec<u8>` returned.
///
/// `data_length` must be exactly the length of data to be written without padding.
///
/// `write_data` is a `Fn` that writes the actual data to a preallocated `Vec<u8>`.
///
/// # Panics
///
/// This function will panic if the call to `write_data` writes a different number of bytes than
/// `data_length`.
pub fn write_bytes<F>(data_length: u16, write_data: F) -> Vec<u8>
where
    F: Fn(&mut Vec<u8>),
{
    // pre-allocate enough space for data + padding
    let total_length = total_padded_length(data_length);
    let mut bytes = Vec::with_capacity(total_length);
    // write data
    write_data(&mut bytes);
    // pad to total length
    assert!(bytes.len() == data_length as usize);
    bytes.resize(total_length, 0);

    bytes
}
