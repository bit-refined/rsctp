use super::{Chunk, GenericTryFromChunkError, TryFrom};

/// Type ID for this chunk.
pub const TYPE_ID: u8 = 10;
/// Minimum size of a `COOKIE_ECHO` chunk. Four bytes chunk header.
const MINIMUM_SIZE: u16 = super::HEADER_SIZE;

/// Implements the `COOKIE_ECHO` chunk.
///
/// This chunk is used only during the initialization of an association. It is sent by the
/// initiator of an association to its peer to complete the initialization process. This chunk MUST
/// precede any `DATA` chunk sent within the association, but MAY be bundled with one or more
/// `DATA` chunks in the same packet.
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct CookieEcho<'bytes> {
    /// The base chunk this specialized chunk is based upon.
    chunk: Chunk<'bytes>,
}

impl<'bytes> TryFrom<Chunk<'bytes>> for CookieEcho<'bytes> {
    type Error = GenericTryFromChunkError;

    fn try_from(chunk: Chunk<'bytes>) -> Result<Self, Self::Error> {
        chunk.verify_min_length(TYPE_ID, MINIMUM_SIZE)?;

        Ok(Self { chunk })
    }
}

impl<'bytes> From<CookieEcho<'bytes>> for Chunk<'bytes> {
    #[allow(clippy::cast_possible_truncation)]
    fn from(cookie_echo: CookieEcho<'bytes>) -> Self {
        cookie_echo.chunk
    }
}

#[cfg(test)]
mod tests {
    use super::{Chunk, CookieEcho, TryFrom, TYPE_ID};

    #[test]
    fn from_chunk() {
        let chunk = Chunk {
            bytes: &[TYPE_ID, 0x00, 0x00, 0x06, 13, 37],
        };

        let _ = CookieEcho::try_from(chunk).unwrap();
    }
}
