use crate::network::generic_tlv::{ParsedTLV, TryTLVFromBytesError, TLV};

use super::{Chunk, GenericTryFromChunkError, TryFrom};

/// Type ID for this chunk.
pub const TYPE_ID: u8 = 6;
/// Minimum size of a `ABORT` chunk. One byte type, another for flags and two bytes for its length.
/// This length also assumes that no error cause is given.
const MINIMUM_SIZE: u16 = super::HEADER_SIZE;

/// Bitmask for the Verification Tag Reflected flag
const FLAG_REFLECTED_VERIFICATION_TAG: u8 = 0b0000_0001;

/// Implements the `ABORT` chunk.
///
/// The `ABORT` chunk is sent to the peer of an association to close the association. The `ABORT`
/// chunk may contain Cause Parameters to inform the receiver about the reason of the abort.
/// `DATA` chunks MUST NOT be bundled with `ABORT`. Control chunks (except for `INIT`, `INIT_ACK`,
/// and `SHUTDOWN_COMPLETE`) MAY be bundled with an `ABORT`, but they MUST be placed before the
/// `ABORT` in the SCTP packet or they will be ignored by the receiver.
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Abort<'bytes> {
    /// The base chunk this specialized chunk is based upon.
    chunk: Chunk<'bytes>,
    /// Zero or more Error Causes defining the cause of the `ABORT`.
    error_causes: Vec<TLV<'bytes>>,
}

impl<'bytes> Abort<'bytes> {
    /// If true, the sent Verification Tag is reflected from the received Verification Tag.
    /// Otherwise, the Verification Tag expected by the peer has to be sent.
    pub const fn reflected_verification_tag(&self) -> bool {
        self.chunk.flag(FLAG_REFLECTED_VERIFICATION_TAG)
    }

    /// Error causes contained in the `Abort` chunk.
    pub const fn error_causes(&self) -> &Vec<TLV<'bytes>> {
        &self.error_causes
    }
}

/// Error returned by `TryFrom` for `Abort`.
#[derive(Debug)]
#[allow(variant_size_differences)]
pub enum TryAbortFromChunkError<'bytes> {
    /// Verification error when trying to convert a generic TLV into a specialized one.
    ChunkError(GenericTryFromChunkError),
    /// Error when trying to convert bytes into a generic TLV.
    TLVParsing(TryTLVFromBytesError),
    /// Result is a valid `Abort` but we encountered parameters that require reporting.
    RequiredReporting(Abort<'bytes>, Vec<TLV<'bytes>>),
}

impl<'bytes> TryFrom<Chunk<'bytes>> for Abort<'bytes> {
    type Error = TryAbortFromChunkError<'bytes>;

    fn try_from(chunk: Chunk<'bytes>) -> Result<Self, Self::Error> {
        chunk.verify_min_length(TYPE_ID, MINIMUM_SIZE)?;

        let parsed_tlv = TLV::from_list(chunk.value(), crate::network::generic_tlv::error::is_recognized)?;

        match parsed_tlv {
            ParsedTLV::AllRecognized(error_causes)
            | ParsedTLV::SomeRecognized(error_causes)
            | ParsedTLV::Stop(error_causes) => Ok(Self { chunk, error_causes }),
            ParsedTLV::SomeRecognizedAndReport(error_causes, reported)
            | ParsedTLV::StopAndReport(error_causes, reported) => Err(TryAbortFromChunkError::RequiredReporting(
                Self { chunk, error_causes },
                reported,
            )),
        }
    }
}

impl<'bytes> From<Abort<'bytes>> for Chunk<'bytes> {
    fn from(abort: Abort<'bytes>) -> Self {
        abort.chunk
    }
}

impl From<GenericTryFromChunkError> for TryAbortFromChunkError<'_> {
    fn from(e: GenericTryFromChunkError) -> Self {
        TryAbortFromChunkError::ChunkError(e)
    }
}

impl From<TryTLVFromBytesError> for TryAbortFromChunkError<'_> {
    fn from(e: TryTLVFromBytesError) -> Self {
        TryAbortFromChunkError::TLVParsing(e)
    }
}

#[cfg(test)]
mod tests {
    use super::{Abort, Chunk, TryFrom, TLV, TYPE_ID};

    #[test]
    fn from_chunk() {
        let chunk = Chunk {
            bytes: &[TYPE_ID, 1, 0, 4],
        };

        let parameter = Abort::try_from(chunk).unwrap();

        assert!(parameter.reflected_verification_tag());
        assert!(parameter.error_causes().is_empty());
    }

    #[test]
    fn from_chunk_with_error_cause() {
        let chunk = Chunk {
            bytes: &[TYPE_ID, 0, 0, 4, 0, 7, 0, 4],
        };

        let parameter = Abort::try_from(chunk).unwrap();

        assert!(!parameter.reflected_verification_tag());

        let tlv = TLV::try_from(&[0, 7, 0, 4][..]).unwrap();

        assert_eq!(&vec![tlv], parameter.error_causes());
    }
}
