use crate::network::generic_tlv::parameter::HeartbeatInfo;
use crate::network::generic_tlv::{GenericTryFromTLVError, ParsedTLV, TryTLVFromBytesError, TLV};

use super::{Chunk, GenericTryFromChunkError, TryFrom};

/// Type ID for this chunk.
pub const TYPE_ID: u8 = 4;
/// Minimum size of this chunk. 4 bytes header, 4 bytes parameter header.
const MINIMUM_SIZE: u16 = super::HEADER_SIZE + super::super::generic_tlv::parameter::HEADER_SIZE;

/// Implements the `HEARTBEAT` chunk.
///
/// An endpoint should send this chunk to its peer endpoint to probe the reachability of a
/// particular destination transport address defined in the present association.
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Heartbeat<'bytes> {
    /// The base chunk this specialized chunk is based upon.
    chunk: Chunk<'bytes>,
    /// Heartbeat Information, a variable-length opaque data structure understood only by the
    /// sender.
    info: HeartbeatInfo<'bytes>,
}

/// Error returned by `TryFrom` for `Heartbeat`.
#[derive(Debug)]
#[allow(variant_size_differences)]
pub enum TryHeartbeatFromChunkError<'bytes> {
    /// Verification error when trying to convert a generic TLV into a specialized one.
    ChunkParsing(GenericTryFromChunkError),
    /// Error when trying to convert bytes into a generic TLV.
    TLVParsing(TryTLVFromBytesError),
    /// Error when trying to parse `HeartbeatInfo`.
    HeartbeatInfoParsing(GenericTryFromTLVError),
    /// Result is a valid `HeartbeatACK` but we encountered parameters that require reporting.
    RequiredReporting(Heartbeat<'bytes>, Vec<TLV<'bytes>>),
    /// We encountered a chunk which has no sender-specific heartbeat info.
    MissingInfo,
    /// A parameter which may only be given once was encountered multiple times.
    MultipleParameters,
}

impl<'bytes> TryFrom<Chunk<'bytes>> for Heartbeat<'bytes> {
    type Error = TryHeartbeatFromChunkError<'bytes>;

    fn try_from(chunk: Chunk<'bytes>) -> Result<Self, Self::Error> {
        chunk.verify_min_length(TYPE_ID, MINIMUM_SIZE)?;

        // TODO: investigate why chunk.parameters(0) does not work here.
        match TLV::from_list(chunk.value(), crate::network::generic_tlv::parameter::is_recognized)? {
            ParsedTLV::AllRecognized(mut params)
            | ParsedTLV::SomeRecognized(mut params)
            | ParsedTLV::Stop(mut params) => {
                #[allow(clippy::option_unwrap_used)]
                let info = match params.len() {
                    0 => return Err(TryHeartbeatFromChunkError::MissingInfo),
                    1 => HeartbeatInfo::try_from(params.pop().unwrap())?,
                    _ => return Err(TryHeartbeatFromChunkError::MultipleParameters),
                };

                Ok(Self { chunk, info })
            }
            ParsedTLV::SomeRecognizedAndReport(mut params, reported)
            | ParsedTLV::StopAndReport(mut params, reported) => {
                #[allow(clippy::option_unwrap_used)]
                let info = match params.len() {
                    0 => return Err(TryHeartbeatFromChunkError::MissingInfo),
                    1 => HeartbeatInfo::try_from(params.pop().unwrap())?,
                    _ => return Err(TryHeartbeatFromChunkError::MultipleParameters),
                };

                Err(TryHeartbeatFromChunkError::RequiredReporting(
                    Self { chunk, info },
                    reported,
                ))
            }
        }
    }
}

impl<'bytes> From<Heartbeat<'bytes>> for Chunk<'bytes> {
    fn from(heartbeat: Heartbeat<'bytes>) -> Self {
        heartbeat.chunk
    }
}

impl From<GenericTryFromChunkError> for TryHeartbeatFromChunkError<'_> {
    fn from(e: GenericTryFromChunkError) -> Self {
        TryHeartbeatFromChunkError::ChunkParsing(e)
    }
}

impl From<TryTLVFromBytesError> for TryHeartbeatFromChunkError<'_> {
    fn from(e: TryTLVFromBytesError) -> Self {
        TryHeartbeatFromChunkError::TLVParsing(e)
    }
}

impl From<GenericTryFromTLVError> for TryHeartbeatFromChunkError<'_> {
    fn from(e: GenericTryFromTLVError) -> Self {
        TryHeartbeatFromChunkError::HeartbeatInfoParsing(e)
    }
}

#[cfg(test)]
mod tests {
    use super::{Chunk, Heartbeat, TryFrom, TYPE_ID};

    #[test]
    fn from_chunk() {
        let chunk = Chunk {
            bytes: &[
                TYPE_ID, 0x00, 0x00, 0x0d,
                /* long comment that is needed so rustfmt doesn't mess up formatting */
                0x00, 0x01, 0x00, 0x09, 0x74, 0x65, 0x73, 0x74, 0x0a,
            ],
        };

        let hearbeat = Heartbeat::try_from(chunk).unwrap();
        let info = hearbeat.info;
        if let Ok(text) = std::str::from_utf8(info.sender_specific_heartbeat_info()) {
            assert_eq!(text, "test\n");
        } else {
            panic!("Failed to parse user data as UTF-8");
        }
    }
}
