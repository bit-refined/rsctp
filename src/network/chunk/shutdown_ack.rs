use super::{Chunk, GenericTryFromChunkError, TryFrom};

/// Type ID for this chunk.
pub const TYPE_ID: u8 = 8;
/// Static size for this chunk. Must always be 4.
const STATIC_SIZE: u16 = 4;

/// Implements the `SHUTDOWN_ACK` chunk.
///
/// This chunk MUST be used to acknowledge the receipt of the `SHUTDOWN` chunk at the completion of
/// the shutdown process.
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct ShutdownACK<'bytes> {
    /// The base chunk this specialized chunk is based upon.
    chunk: Chunk<'bytes>,
}

impl<'bytes> TryFrom<Chunk<'bytes>> for ShutdownACK<'bytes> {
    type Error = GenericTryFromChunkError;

    fn try_from(chunk: Chunk<'bytes>) -> Result<Self, Self::Error> {
        chunk.verify_static_length(TYPE_ID, STATIC_SIZE)?;

        Ok(Self { chunk })
    }
}

impl<'bytes> From<ShutdownACK<'bytes>> for Chunk<'bytes> {
    fn from(cookie_ack: ShutdownACK<'bytes>) -> Self {
        cookie_ack.chunk
    }
}

#[cfg(test)]
mod tests {
    use super::{Chunk, ShutdownACK, TryFrom, TYPE_ID};

    #[test]
    fn from_chunk() {
        let chunk = Chunk {
            bytes: &[TYPE_ID, 0, 0, 4],
        };

        let _ = ShutdownACK::try_from(chunk).unwrap();
    }
}
