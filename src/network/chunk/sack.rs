use byteorder::{ByteOrder, NetworkEndian};

use super::{Chunk, GenericTryFromChunkError, TryFrom};

/// Type ID for this chunk.
pub const TYPE_ID: u8 = 3;
/// Length of the fixed parameters of a `SACK` chunk.
const FIXED_PARAMETER_SIZE: u16 = 12;
/// Minimum size of a `SACK` chunk.
const MINIMUM_SIZE: u16 = super::HEADER_SIZE + FIXED_PARAMETER_SIZE;

/// Implements the `SACK` chunk.
///
/// This chunk is sent to the peer endpoint to acknowledge received `DATA` chunks and to inform the
/// peer endpoint of gaps in the received subsequences of `DATA` chunks as represented by their
/// TSNs.
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct SACK<'bytes> {
    /// The base chunk this specialized chunk is based upon.
    chunk: Chunk<'bytes>,
}

impl<'bytes> SACK<'bytes> {
    /// This parameter contains the TSN of the last `DATA` chunk received in sequence before a gap.
    /// In the case where no `DATA` chunk has been received, this value is set to the peer's
    /// Initial TSN minus one.
    pub fn cumulative_transmission_sequence_number_ack(&self) -> u32 {
        #[allow(clippy::indexing_slicing)]
        NetworkEndian::read_u32(&self.chunk.value()[0..=3])
    }

    /// This field indicates the updated receive buffer space in bytes of the sender of this
    /// `SACK`.
    pub fn advertised_receiver_window_credit(&self) -> u32 {
        #[allow(clippy::indexing_slicing)]
        NetworkEndian::read_u32(&self.chunk.value()[4..=7])
    }

    /// Indicates the number of Gap Ack Blocks included in this `SACK`.
    pub fn number_of_gap_ack_blocks(&self) -> u16 {
        #[allow(clippy::indexing_slicing)]
        NetworkEndian::read_u16(&self.chunk.value()[8..=9])
    }

    /// This field contains the number of duplicate TSNs the endpoint has received. Each duplicate
    /// TSN is listed following the Gap Ack Block list.
    pub fn number_of_duplicate_transmission_sequence_numbers(&self) -> u16 {
        #[allow(clippy::indexing_slicing)]
        NetworkEndian::read_u16(&self.chunk.value()[10..=11])
    }

    /// These fields contain the Gap Ack Blocks. They are repeated for each Gap Ack Block up to the
    /// number of Gap Ack Blocks defined in the Number of Gap Ack Blocks field. All `DATA` chunks
    /// with TSNs greater than or equal to (Cumulative TSN Ack + Gap Ack Block Start) and less than
    /// or equal to (Cumulative TSN Ack + Gap Ack Block End) of each Gap Ack Block are assumed to
    /// have been received correctly.
    #[allow(clippy::indexing_slicing, clippy::integer_arithmetic)]
    pub fn gap_ack_blocks(&self) -> impl Iterator<Item = (u16, u16)> + 'bytes {
        let gap_blocks_start = MINIMUM_SIZE as usize;
        let gap_blocks_end = gap_blocks_start + 4 * self.number_of_gap_ack_blocks() as usize;

        self.chunk.bytes[gap_blocks_start..gap_blocks_end]
            .chunks_exact(4)
            .map(|chunk| {
                (
                    NetworkEndian::read_u16(&chunk[0..=1]),
                    NetworkEndian::read_u16(&chunk[2..=3]),
                )
            })
    }

    /// Indicates the number of times a TSN was received in duplicate since the last `SACK` was
    /// sent. Every time a receiver gets a duplicate TSN (before sending the `SACK`), it adds it to
    /// the list of duplicates. The duplicate count is reinitialized to zero after sending each
    /// `SACK`.
    #[allow(clippy::indexing_slicing, clippy::integer_arithmetic)]
    pub fn duplicate_transmission_sequence_numbers(&self) -> impl Iterator<Item = u32> + 'bytes {
        let duplicate_tsn_start = (MINIMUM_SIZE + 4 * self.number_of_gap_ack_blocks()) as usize;
        let duplicate_tsn_end =
            duplicate_tsn_start + 4 * self.number_of_duplicate_transmission_sequence_numbers() as usize;

        self.chunk.bytes[duplicate_tsn_start..duplicate_tsn_end]
            .chunks_exact(4)
            .map(|chunk| NetworkEndian::read_u32(&chunk[0..=3]))
    }
}

impl<'bytes> TryFrom<Chunk<'bytes>> for SACK<'bytes> {
    type Error = GenericTryFromChunkError;

    fn try_from(chunk: Chunk<'bytes>) -> Result<Self, Self::Error> {
        chunk.verify_min_length(TYPE_ID, MINIMUM_SIZE)?;

        let length = chunk.length();
        let sack = Self { chunk };

        let gap_ack_blocks_size = sack
            .number_of_gap_ack_blocks()
            .checked_mul(4)
            .ok_or(GenericTryFromChunkError::InvalidLength)?;

        let duplicate_tsn_size = sack
            .number_of_duplicate_transmission_sequence_numbers()
            .checked_mul(4)
            .ok_or(GenericTryFromChunkError::InvalidLength)?;

        let calculated_size = Some(MINIMUM_SIZE)
            .and_then(|x| x.checked_add(gap_ack_blocks_size))
            .and_then(|x| x.checked_add(duplicate_tsn_size))
            .ok_or(GenericTryFromChunkError::InvalidLength)?;

        if calculated_size != length {
            return Err(GenericTryFromChunkError::InvalidLength);
        }

        Ok(sack)
    }
}

impl<'bytes> From<SACK<'bytes>> for Chunk<'bytes> {
    fn from(shutdown: SACK<'bytes>) -> Self {
        shutdown.chunk
    }
}

#[cfg(test)]
mod tests {
    use super::{Chunk, TryFrom, SACK, TYPE_ID};

    #[test]
    fn from_chunk() {
        let chunk = Chunk {
            bytes: &[
                TYPE_ID, 0, 0, 28, // chunk header
                0, 0, 0, 5, // cumulative tsn ack
                0, 0, 0, 7, // advertised receiver window credit
                0, 1, 0, 2, // gap ack blocks & duplicate tsn
                0, 3, 0, 6, // gap ack block #1
                0, 0, 0, 10, // duplicate tsn #1
                0, 0, 0, 11, // duplicate tsn #1
            ],
        };

        let chunk = SACK::try_from(chunk).unwrap();

        assert_eq!(5, chunk.cumulative_transmission_sequence_number_ack());
        assert_eq!(7, chunk.advertised_receiver_window_credit());
        assert_eq!(1, chunk.number_of_gap_ack_blocks());
        assert_eq!(2, chunk.number_of_duplicate_transmission_sequence_numbers());
        assert_eq!(vec![(3, 6)], chunk.gap_ack_blocks().collect::<Vec<_>>());
        assert_eq!(
            vec![10, 11],
            chunk.duplicate_transmission_sequence_numbers().collect::<Vec<_>>()
        );
    }
}
