use crate::network::chunk::GenericTryFromChunkError;
use crate::network::generic_tlv::parameter::unrecognized_parameters::TryUnrecognizedParametersFromTLVError;
use crate::network::generic_tlv::parameter::{
    explicit_congestion_notification::TYPE_ID as TYPE_ID_EXPLICIT_CONGESTION_NOTIFICATION,
    host_name_address::TryHostNameAddressFromTLVError, host_name_address::TYPE_ID as TYPE_ID_HOST_NAME_ADDRESS,
    ipv4::TYPE_ID as TYPE_ID_IPV4, ipv6::TYPE_ID as TYPE_ID_IPV6, state_cookie::TYPE_ID as TYPE_ID_STATE_COOKIE,
    unrecognized_parameters::TYPE_ID as TYPE_ID_UNRECOGNIZED_PARAMETER, ExplicitCongestionNotification,
    HostNameAddress, IPv4, IPv6, StateCookie, UnrecognizedParameters,
};
use crate::network::generic_tlv::{GenericTryFromTLVError, ParsedTLV, TryTLVFromBytesError, TLV};

use super::{Chunk, TryFrom};

// IMPORTANT!! -> Changing this requires changes to init.rs!
/// Type ID for this chunk.
pub const TYPE_ID: u8 = 2;
/// Length of the fixed parameters of an `INIT_ACK` chunk.
const FIXED_PARAMETER_SIZE: u16 = 16;
/// Minimum size of an `INIT_ACk` chunk.
const MINIMUM_SIZE: u16 = super::HEADER_SIZE + FIXED_PARAMETER_SIZE;

/// Implements the `INIT_ACK` chunk.
///
/// The INIT ACK chunk is used to acknowledge the initiation of an SCTP association.
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct InitACK<'bytes> {
    /// The base chunk this specialized chunk is based upon.
    chunk: Chunk<'bytes>,
    /// Necessary state and parameter information for the sender of this `INIT_ACK` to create the
    /// association.
    state_cookie: StateCookie<'bytes>,
    /// This parameter lists all the IPv4 addresses used at the sending endpoint. If it is a
    /// multihomed connection then the IP address of each may be included.
    list_ipv4: Vec<IPv4<'bytes>>,
    /// This parameter lists all the IPv6 addresses used at the sending endpoint. If it is a
    /// multihomed connection then the IP address of each may be included.
    list_ipv6: Vec<IPv6<'bytes>>,
    /// Parameters in the `INIT` chunk that the receiver didn't understand.
    unrecognized_parameters: Option<UnrecognizedParameters<'bytes>>,
    /// This parameter is a hostname as defined in RFC 1123, section 2.1.
    host_name_address: Option<HostNameAddress<'bytes>>,
    /// This parameter is reserved for explicit congestion notification support.
    explicit_congestion_notification: Option<ExplicitCongestionNotification<'bytes>>,
}

/// Error returned by `TryFrom` for `InitACK`.
#[allow(variant_size_differences)]
pub enum TryInitACKFromChunkError<'bytes> {
    /// Verification error when trying to convert a generic TLV into a specialized one.
    ChunkParsing(GenericTryFromChunkError),
    /// Error when trying to convert bytes into a generic TLV.
    TLVParsing(TryTLVFromBytesError),
    /// Error when trying to convert a generic TLV into a specialized one.
    ParameterParsing(GenericTryFromTLVError),
    /// Error when trying to parse a `HostNameAddress` parameter.
    HostNameParsing(TryHostNameAddressFromTLVError),
    /// Error when trying to parse `UnrecognizedParameters` parameter.
    UnrecognizedParametersParsing(TryUnrecognizedParametersFromTLVError),
    /// Result is a valid `InitACK` but we encountered parameters that require reporting.
    RequiredReporting(InitACK<'bytes>, Vec<TLV<'bytes>>),
    /// A parameter which may only be given once was encountered multiple times.
    MultipleParameters(u16),
    /// A parameter which may never be given has been encountered.
    WrongParameter(u16),
    /// The required `StateCookie` is missing.
    MissingStateCookie,
}

impl<'bytes> TryFrom<Chunk<'bytes>> for InitACK<'bytes> {
    type Error = TryInitACKFromChunkError<'bytes>;

    fn try_from(chunk: Chunk<'bytes>) -> Result<Self, Self::Error> {
        chunk.verify_min_length(TYPE_ID, MINIMUM_SIZE)?;

        // TODO: investigate why chunk.parameters(16) does not work here.
        #[allow(clippy::indexing_slicing)]
        let tlvs = TLV::from_list(
            &chunk.value()[16..],
            crate::network::generic_tlv::parameter::is_recognized,
        )?;

        let (params, unrecognized) = match tlvs {
            ParsedTLV::AllRecognized(params) | ParsedTLV::SomeRecognized(params) | ParsedTLV::Stop(params) => {
                (params, None)
            }
            ParsedTLV::SomeRecognizedAndReport(params, reported) | ParsedTLV::StopAndReport(params, reported) => {
                (params, Some(reported))
            }
        };

        let mut list_ipv4 = Vec::new();
        let mut list_ipv6 = Vec::new();
        let mut state_cookie = None;
        let mut unrecognized_parameters = None;
        let mut host_name_address = None;
        let mut explicit_congestion_notification = None;

        for param in params {
            match param.ty() {
                TYPE_ID_IPV4 => list_ipv4.push(IPv4::try_from(param)?),
                TYPE_ID_IPV6 => list_ipv6.push(IPv6::try_from(param)?),
                TYPE_ID_STATE_COOKIE => {
                    if let Some(_p) = state_cookie.replace(StateCookie::try_from(param)?) {
                        return Err(TryInitACKFromChunkError::MultipleParameters(TYPE_ID_STATE_COOKIE));
                    }
                }
                TYPE_ID_UNRECOGNIZED_PARAMETER => {
                    if let Some(_p) = unrecognized_parameters.replace(UnrecognizedParameters::try_from(param)?) {
                        return Err(TryInitACKFromChunkError::MultipleParameters(
                            TYPE_ID_UNRECOGNIZED_PARAMETER,
                        ));
                    }
                }
                TYPE_ID_HOST_NAME_ADDRESS => {
                    if let Some(_p) = host_name_address.replace(HostNameAddress::try_from(param)?) {
                        return Err(TryInitACKFromChunkError::MultipleParameters(TYPE_ID_HOST_NAME_ADDRESS));
                    }
                }
                TYPE_ID_EXPLICIT_CONGESTION_NOTIFICATION => {
                    if let Some(_p) =
                        explicit_congestion_notification.replace(ExplicitCongestionNotification::try_from(param)?)
                    {
                        return Err(TryInitACKFromChunkError::MultipleParameters(
                            TYPE_ID_EXPLICIT_CONGESTION_NOTIFICATION,
                        ));
                    }
                }
                ty => return Err(TryInitACKFromChunkError::WrongParameter(ty)),
            }
        }

        let init_ack = Self {
            chunk,
            state_cookie: state_cookie.ok_or(TryInitACKFromChunkError::MissingStateCookie)?,
            list_ipv4,
            list_ipv6,
            unrecognized_parameters: None,
            host_name_address,
            explicit_congestion_notification,
        };

        if let Some(unrecognized) = unrecognized {
            Err(TryInitACKFromChunkError::RequiredReporting(init_ack, unrecognized))
        } else {
            Ok(init_ack)
        }
    }
}

impl<'bytes> From<InitACK<'bytes>> for Chunk<'bytes> {
    fn from(init: InitACK<'bytes>) -> Self {
        init.chunk
    }
}

impl From<GenericTryFromChunkError> for TryInitACKFromChunkError<'_> {
    fn from(e: GenericTryFromChunkError) -> Self {
        TryInitACKFromChunkError::ChunkParsing(e)
    }
}

impl From<TryTLVFromBytesError> for TryInitACKFromChunkError<'_> {
    fn from(e: TryTLVFromBytesError) -> Self {
        TryInitACKFromChunkError::TLVParsing(e)
    }
}

impl From<GenericTryFromTLVError> for TryInitACKFromChunkError<'_> {
    fn from(e: GenericTryFromTLVError) -> Self {
        TryInitACKFromChunkError::ParameterParsing(e)
    }
}

impl From<TryHostNameAddressFromTLVError> for TryInitACKFromChunkError<'_> {
    fn from(e: TryHostNameAddressFromTLVError) -> Self {
        TryInitACKFromChunkError::HostNameParsing(e)
    }
}

impl From<TryUnrecognizedParametersFromTLVError> for TryInitACKFromChunkError<'_> {
    fn from(e: TryUnrecognizedParametersFromTLVError) -> Self {
        TryInitACKFromChunkError::UnrecognizedParametersParsing(e)
    }
}
