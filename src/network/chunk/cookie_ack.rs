use super::{Chunk, GenericTryFromChunkError, TryFrom};

/// Type ID for this chunk.
pub const TYPE_ID: u8 = 11;
/// Static size for this chunk. Must always be 4.
const STATIC_SIZE: u16 = 4;

/// Implements the `COOKIE_ACK` chunk.
///
/// This chunk is used only during the initialization of an association. It is used to acknowledge
/// the receipt of a `COOKIE_ECHO` chunk. This chunk MUST precede any `DATA` or `SACK` chunk sent
/// within the association, but MAY be bundled with one or more `DATA` chunks or `SACK` chunks in
/// the same SCTP packet.
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct CookieACK<'bytes> {
    /// The base chunk this specialized chunk is based upon.
    chunk: Chunk<'bytes>,
}

impl<'bytes> TryFrom<Chunk<'bytes>> for CookieACK<'bytes> {
    type Error = GenericTryFromChunkError;

    fn try_from(chunk: Chunk<'bytes>) -> Result<Self, Self::Error> {
        chunk.verify_static_length(TYPE_ID, STATIC_SIZE)?;

        Ok(Self { chunk })
    }
}

impl<'bytes> From<CookieACK<'bytes>> for Chunk<'bytes> {
    fn from(cookie_ack: CookieACK<'bytes>) -> Self {
        cookie_ack.chunk
    }
}

#[cfg(test)]
mod tests {
    use super::{Chunk, CookieACK, TryFrom, TYPE_ID};

    #[test]
    fn from_chunk() {
        let chunk = Chunk {
            bytes: &[TYPE_ID, 0, 0, 4],
        };

        let _ = CookieACK::try_from(chunk).unwrap();
    }
}
