use super::{Chunk, GenericTryFromChunkError, TryFrom};

/// Type ID for this chunk.
pub const TYPE_ID: u8 = 14;
/// Static size for this chunk as a `SHUTDOWN_COMPLETE` chunk MUST always be 4 bytes in size.
const STATIC_SIZE: u16 = 4;
/// Bitmask for the Verification Tag Reflected flag.
const FLAG_REFLECTED_VERIFICATION_TAG: u8 = 0b0000_0001;

/// Implements the `SHUTDOWN_COMPLETE` chunk.
///
/// This chunk MUST be used to acknowledge the receipt of a `SHUTDOWN_ACK` chunk.
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct ShutdownComplete<'bytes> {
    /// The base chunk this specialized chunk is based upon.
    chunk: Chunk<'bytes>,
}

impl ShutdownComplete<'_> {
    /// The Verification Tag Reflected flag.
    ///
    /// If we have a TCB, this flag MUST be set to false and the peer's verfication tag has to be
    /// sent. If we do not have a TCB, this flag MUST be set to true and the verification tag from
    /// the corresponding `SHUTDOWN_ACK` chunk has to be reflected back to the receiver.
    pub const fn verification_tag_reflected(&self) -> bool {
        self.chunk.flag(FLAG_REFLECTED_VERIFICATION_TAG)
    }
}

impl<'bytes> TryFrom<Chunk<'bytes>> for ShutdownComplete<'bytes> {
    type Error = GenericTryFromChunkError;

    fn try_from(chunk: Chunk<'bytes>) -> Result<Self, Self::Error> {
        chunk.verify_static_length(TYPE_ID, STATIC_SIZE)?;

        Ok(Self { chunk })
    }
}

impl<'bytes> From<ShutdownComplete<'bytes>> for Chunk<'bytes> {
    fn from(shutdown_complete: ShutdownComplete<'bytes>) -> Self {
        shutdown_complete.chunk
    }
}

#[cfg(test)]
mod tests {
    use super::{Chunk, ShutdownComplete, TryFrom, TYPE_ID};

    #[test]
    fn from_chunk() {
        let chunk = Chunk {
            bytes: &[TYPE_ID, 1, 0, 4],
        };

        let chunk = ShutdownComplete::try_from(chunk).unwrap();

        assert!(chunk.verification_tag_reflected());
    }
}
