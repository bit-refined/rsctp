use byteorder::{ByteOrder, NetworkEndian};

use super::{Chunk, GenericTryFromChunkError, TryFrom};

/// Type ID for this chunk.
pub const TYPE_ID: u8 = 7;
/// Static size for this chunk. Must always be 8.
const STATIC_SIZE: u16 = 8;

/// Implements the `SHUTDOWN` chunk.
///
/// An endpoint in an association MUST use this chunk to initiate a graceful close of the
/// association with its peer.
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Shutdown<'bytes> {
    /// The base chunk this specialized chunk is based upon.
    chunk: Chunk<'bytes>,
}

impl Shutdown<'_> {
    /// This parameter contains the TSN of the last chunk received in sequence before any gaps.
    pub fn cumulative_transmission_sequence_number_ack(&self) -> u32 {
        #[allow(clippy::indexing_slicing)]
        NetworkEndian::read_u32(self.chunk.value())
    }
}

impl<'bytes> TryFrom<Chunk<'bytes>> for Shutdown<'bytes> {
    type Error = GenericTryFromChunkError;

    fn try_from(chunk: Chunk<'bytes>) -> Result<Self, Self::Error> {
        chunk.verify_static_length(TYPE_ID, STATIC_SIZE)?;

        Ok(Self { chunk })
    }
}

impl<'bytes> From<Shutdown<'bytes>> for Chunk<'bytes> {
    fn from(shutdown: Shutdown<'bytes>) -> Self {
        shutdown.chunk
    }
}

#[cfg(test)]
mod tests {
    use super::{Chunk, Shutdown, TryFrom, TYPE_ID};

    #[test]
    fn from_chunk() {
        let chunk = Chunk {
            bytes: &[TYPE_ID, 0, 0, 8, 0, 0, 0x05, 0x39],
        };

        let chunk = Shutdown::try_from(chunk).unwrap();

        assert_eq!(1337, chunk.cumulative_transmission_sequence_number_ack());
    }
}
