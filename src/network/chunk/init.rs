use byteorder::{ByteOrder, NetworkEndian};

use crate::network::{
    chunk::GenericTryFromChunkError,
    generic_tlv::parameter::{
        cookie_preservative::TYPE_ID as TYPE_ID_COOKIE_PRESERVATIVE,
        explicit_congestion_notification::TYPE_ID as TYPE_ID_EXPLICIT_CONGESTION_NOTIFICATION,
        host_name_address::TryHostNameAddressFromTLVError, host_name_address::TYPE_ID as TYPE_ID_HOST_NAME_ADDRESS,
        ipv4::TYPE_ID as TYPE_ID_IPV4, ipv6::TYPE_ID as TYPE_ID_IPV6,
        supported_address_types::TrySupportedAddressTypesFromTLVError,
        supported_address_types::TYPE_ID as TYPE_ID_SUPPORTED_ADDRESS_TYPES, CookiePreservative,
        ExplicitCongestionNotification, HostNameAddress, IPv4, IPv6, SupportedAddressTypes,
    },
    generic_tlv::{GenericTryFromTLVError, ParsedTLV, TryTLVFromBytesError, TLV},
};

use super::{Chunk, TryFrom};

// IMPORTANT!! -> Changing this requires changes to init_ack.rs!
/// Type ID for this chunk.
pub const TYPE_ID: u8 = 1;
/// Length of the fixed parameters of an `INIT` chunk.
const FIXED_PARAMETER_SIZE: u16 = 16;
/// Minimum size of an `INIT` chunk.
const MINIMUM_SIZE: u16 = super::HEADER_SIZE + FIXED_PARAMETER_SIZE;

/// Implements the `INIT` chunk.
///
/// This chunk is used to initiate an SCTP association between two endpoints.
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Init<'bytes> {
    /// The base chunk this specialized chunk is based upon.
    chunk: Chunk<'bytes>,
    /// This parameter lists all the IPv4 addresses used at the sending endpoint. If it is a
    /// multihomed connection then the IP address of each may be included.
    list_ipv4: Vec<IPv4<'bytes>>,
    /// This parameter lists all the IPv6 addresses used at the sending endpoint. If it is a
    /// multihomed connection then the IP address of each may be included.
    list_ipv6: Vec<IPv6<'bytes>>,
    /// This parameter provides a suggested life-span increment the receiver should add to its
    /// default cookie life-span (in milliseconds).
    cookie_preservative: Option<CookiePreservative<'bytes>>,
    /// This parameter is a hostname as defined in RFC 1123, section 2.1.
    host_name_address: Option<HostNameAddress<'bytes>>,
    /// The sender of `INIT` uses this parameter to list all the address types it can support.
    supported_address_types: Option<SupportedAddressTypes<'bytes>>,
    /// This parameter is reserved for explicit congestion notification support.
    explicit_congestion_notification: Option<ExplicitCongestionNotification<'bytes>>,
}

impl<'bytes> Init<'bytes> {
    /// Get the Initiate Tag.
    ///
    /// The receiver of the INIT (the responding end) records the value of the Initiate Tag
    /// parameter. This value MUST be placed into the Verification Tag field of every SCTP packet
    /// that the receiver of the INIT transmits within this association.
    pub fn initiate_tag(&self) -> u32 {
        #[allow(clippy::indexing_slicing)]
        NetworkEndian::read_u32(&self.chunk.value()[0..=3])
    }

    /// Get the Advertised Receiver Window Credit.
    ///
    /// This value represents the dedicated buffer space, in number of bytes, the sender of the
    /// `INIT` has reserved in association with this window.
    pub fn advertised_receiver_window_credit(&self) -> u32 {
        #[allow(clippy::indexing_slicing)]
        NetworkEndian::read_u32(&self.chunk.value()[4..=7])
    }

    /// Get the Number of Outbound Streams.
    ///
    /// Defines the number of outbound streams the sender of this `INIT` chunk wishes to create in
    /// this association. The value of 0 MUST NOT be used.
    pub fn number_of_outbound_streams(&self) -> u16 {
        #[allow(clippy::indexing_slicing)]
        NetworkEndian::read_u16(&self.chunk.value()[8..=9])
    }

    /// Get the Number of Inbound Streams.
    ///
    /// Defines the maximum number of streams the sender of this `INIT` chunk allows the peer end
    /// to create in this association. The value 0 MUST NOT be used.
    pub fn number_of_inbound_streams(&self) -> u16 {
        #[allow(clippy::indexing_slicing)]
        NetworkEndian::read_u16(&self.chunk.value()[10..=11])
    }

    /// Get the Initial Transmission Sequence Number.
    ///
    /// Defines the initial TSN that the sender will use. The valid range is from 0 to 4294967295.
    /// This field MAY be set to the value of the Initiate Tag field.
    pub fn initial_transmission_sequence_number(&self) -> u32 {
        #[allow(clippy::indexing_slicing)]
        NetworkEndian::read_u32(&self.chunk.value()[12..=15])
    }

    /// Get the list of IPv4 Address parameters.
    ///
    /// Contains an IPv4 address of the sending endpoint. It is binary encoded.
    pub const fn list_ipv4(&self) -> &Vec<IPv4<'bytes>> {
        &self.list_ipv4
    }

    /// Get the list of IPv6 Address parameters.
    ///
    /// Contains an IPv6 address of the sending endpoint. It is binary encoded.
    pub const fn list_ipv6(&self) -> &Vec<IPv6<'bytes>> {
        &self.list_ipv6
    }

    /// Get the optional Cookie Preservative parameter.
    ///
    /// The sender of the `INIT` shall use this parameter to suggest to the receiver of the `INIT`
    /// for a longer life-span of the State Cookie.
    pub const fn cookie_preservative(&self) -> &Option<CookiePreservative<'bytes>> {
        &self.cookie_preservative
    }

    /// Get the optional Host Name Address parameter.
    ///
    /// The sender of `INIT` uses this parameter to pass its Host Name (in place of its IP
    /// addresses) to its peer. The peer is responsible for resolving the name. Using this
    /// parameter might make it more likely for the association to work across a NAT box.
    pub const fn host_name_address(&self) -> &Option<HostNameAddress<'bytes>> {
        &self.host_name_address
    }

    /// Get the optional Supported Address Types parameter.
    ///
    /// The sender of INIT uses this parameter to list all the address types it can support.
    pub const fn supported_address_types(&self) -> &Option<SupportedAddressTypes<'bytes>> {
        &self.supported_address_types
    }

    /// Get the optional Explicit Congestion Notification parameter.
    ///
    /// To indicate that an endpoint is ECN capable, an endpoint SHOULD add to the `INIT` and or
    /// `INIT_ACK` chunk the TLV reserved for ECN.#
    pub const fn explicit_congestion_notification(&self) -> &Option<ExplicitCongestionNotification<'bytes>> {
        &self.explicit_congestion_notification
    }
}

/// Error returned by `TryFrom` for `Init`.
#[allow(variant_size_differences)]
pub enum TryInitFromChunkError<'bytes> {
    /// Verification error when trying to convert a generic TLV into a specialized one.
    ChunkParsing(GenericTryFromChunkError),
    /// Error when trying to convert bytes into a generic TLV.
    TLVParsing(TryTLVFromBytesError),
    /// Error when trying to convert a generic TLV into a specialized one.
    ParameterParsing(GenericTryFromTLVError),
    /// Error when trying to parse a `HostNameAddress` parameter.
    HostNameParsing(TryHostNameAddressFromTLVError),
    /// Error when trying to parse a `SupportedAddressTypes` parameter.
    SupportedAddressTypesParsing(TrySupportedAddressTypesFromTLVError),
    /// Result is a valid `Init` but we encountered parameters that require reporting.
    RequiredReporting(Init<'bytes>, Vec<TLV<'bytes>>),
    /// A parameter which may only be given once was encountered multiple times.
    MultipleParameters(u16),
    /// A parameter which may never be given has been encountered.
    WrongParameter(u16),
}

impl<'bytes> TryFrom<Chunk<'bytes>> for Init<'bytes> {
    type Error = TryInitFromChunkError<'bytes>;

    fn try_from(chunk: Chunk<'bytes>) -> Result<Self, Self::Error> {
        chunk.verify_min_length(TYPE_ID, MINIMUM_SIZE)?;

        // TODO: investigate why chunk.parameters(16) does not work here.
        #[allow(clippy::indexing_slicing)]
        let tlvs = TLV::from_list(
            &chunk.value()[16..],
            crate::network::generic_tlv::parameter::is_recognized,
        )?;

        let (params, unrecognized) = match tlvs {
            ParsedTLV::AllRecognized(params) | ParsedTLV::SomeRecognized(params) | ParsedTLV::Stop(params) => {
                (params, None)
            }
            ParsedTLV::SomeRecognizedAndReport(params, reported) | ParsedTLV::StopAndReport(params, reported) => {
                (params, Some(reported))
            }
        };

        let mut list_ipv4 = Vec::new();
        let mut list_ipv6 = Vec::new();
        let mut cookie_preservative = None;
        let mut host_name_address = None;
        let mut supported_address_types = None;
        let mut explicit_congestion_notification = None;

        for param in params {
            match param.ty() {
                TYPE_ID_IPV4 => list_ipv4.push(IPv4::try_from(param)?),
                TYPE_ID_IPV6 => list_ipv6.push(IPv6::try_from(param)?),
                TYPE_ID_COOKIE_PRESERVATIVE => {
                    if let Some(_p) = cookie_preservative.replace(CookiePreservative::try_from(param)?) {
                        return Err(TryInitFromChunkError::MultipleParameters(TYPE_ID_COOKIE_PRESERVATIVE));
                    }
                }
                TYPE_ID_HOST_NAME_ADDRESS => {
                    if let Some(_p) = host_name_address.replace(HostNameAddress::try_from(param)?) {
                        return Err(TryInitFromChunkError::MultipleParameters(TYPE_ID_HOST_NAME_ADDRESS));
                    }
                }
                TYPE_ID_SUPPORTED_ADDRESS_TYPES => {
                    if let Some(_p) = supported_address_types.replace(SupportedAddressTypes::try_from(param)?) {
                        return Err(TryInitFromChunkError::MultipleParameters(
                            TYPE_ID_SUPPORTED_ADDRESS_TYPES,
                        ));
                    }
                }
                TYPE_ID_EXPLICIT_CONGESTION_NOTIFICATION => {
                    if let Some(_p) =
                        explicit_congestion_notification.replace(ExplicitCongestionNotification::try_from(param)?)
                    {
                        return Err(TryInitFromChunkError::MultipleParameters(
                            TYPE_ID_EXPLICIT_CONGESTION_NOTIFICATION,
                        ));
                    }
                }
                ty => return Err(TryInitFromChunkError::WrongParameter(ty)),
            }
        }

        let init = Self {
            chunk,
            list_ipv4,
            list_ipv6,
            cookie_preservative,
            host_name_address,
            supported_address_types,
            explicit_congestion_notification,
        };

        if let Some(unrecognized) = unrecognized {
            Err(TryInitFromChunkError::RequiredReporting(init, unrecognized))
        } else {
            Ok(init)
        }
    }
}

impl<'bytes> From<Init<'bytes>> for Chunk<'bytes> {
    fn from(init: Init<'bytes>) -> Self {
        init.chunk
    }
}

impl From<GenericTryFromChunkError> for TryInitFromChunkError<'_> {
    fn from(e: GenericTryFromChunkError) -> Self {
        TryInitFromChunkError::ChunkParsing(e)
    }
}

impl From<TryTLVFromBytesError> for TryInitFromChunkError<'_> {
    fn from(e: TryTLVFromBytesError) -> Self {
        TryInitFromChunkError::TLVParsing(e)
    }
}

impl From<GenericTryFromTLVError> for TryInitFromChunkError<'_> {
    fn from(e: GenericTryFromTLVError) -> Self {
        TryInitFromChunkError::ParameterParsing(e)
    }
}

impl From<TryHostNameAddressFromTLVError> for TryInitFromChunkError<'_> {
    fn from(e: TryHostNameAddressFromTLVError) -> Self {
        TryInitFromChunkError::HostNameParsing(e)
    }
}

impl From<TrySupportedAddressTypesFromTLVError> for TryInitFromChunkError<'_> {
    fn from(e: TrySupportedAddressTypesFromTLVError) -> Self {
        TryInitFromChunkError::SupportedAddressTypesParsing(e)
    }
}
