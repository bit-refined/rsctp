use byteorder::{ByteOrder, NetworkEndian};

use super::{Chunk, GenericTryFromChunkError, TryFrom};

/// Type ID for this chunk.
pub const TYPE_ID: u8 = 0;
/// Length of the fixed parameters of a `DATA` chunk.
const FIXED_PARAMETER_SIZE: u16 = 12;
/// Minimum size of a `DATA` chunk. Includes at least 1 byte of data.
const MINIMUM_SIZE: u16 = super::HEADER_SIZE + FIXED_PARAMETER_SIZE + 1;

/// Bitmask for the Immediate flag
const FLAG_IMMEDIATE: u8 = 0b0000_1000;
/// Bitmask for the Unordered flag
const FLAG_UNORDERED: u8 = 0b0000_0100;
/// Bitmask for the Beginning flag
const FLAG_BEGINNING: u8 = 0b0000_0010;
/// Bitmask for the End flag
const FLAG_END: u8 = 0b0000_0001;

/// Fragmentation of a chunk
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum Fragmentation {
    /// Given `DATA` chunk is the first fragment of a user message.
    FirstFragment,
    /// Given `DATA` chunk is a middle fragment of a user message.
    MiddleFragment,
    /// Given `DATA` chunk is the last fragment of a user message.
    LastFragment,
    /// Given `DATA` chunk contains the entire, unfragmented user message.
    Unfragmented,
}

#[derive(Clone, Debug, PartialEq, Eq)]
/// Implements the `DATA` chunk.
pub struct Data<'bytes> {
    /// The base chunk this specialized chunk is based upon.
    chunk: Chunk<'bytes>,
}

impl<'bytes> Data<'bytes> {
    /// If this is true, the receiver of this `DATA` chunk SHOULD immediately respond with the
    /// corresponding `SACK` chunk.
    pub const fn should_send_sack_immediately(&self) -> bool {
        self.chunk.flag(FLAG_IMMEDIATE)
    }

    /// If this is true, this is an unordered `DATA` chunk and there is no Stream Sequence Number
    /// assigned to this `DATA` chunk. Therefore, the receiver MUST ignore the Stream Sequence
    /// Number field.
    ///
    /// After reassembly (if necessary), unordered `DATA` chunks MUST be dispatched to the upper
    /// layer by the receiver without any attempt to reorder.
    ///
    /// If an unordered user message is fragmented, each fragment of the message MUST have its
    /// Unordered flag set to true.
    pub const fn is_unordered(&self) -> bool {
        self.chunk.flag(FLAG_UNORDERED)
    }

    /// If this is true, this data chunk is the beginning of a fragmented user message.
    const fn is_beginning(&self) -> bool {
        self.chunk.flag(FLAG_BEGINNING)
    }

    /// If this is true, this data chunk is the end of a fragmented user message.
    const fn is_end(&self) -> bool {
        self.chunk.flag(FLAG_END)
    }

    /// Get the fragmentation of this `DATA` chunk.
    pub fn fragmentation(&self) -> Fragmentation {
        match (self.is_beginning(), self.is_end()) {
            (true, false) => Fragmentation::FirstFragment,
            (false, false) => Fragmentation::MiddleFragment,
            (false, true) => Fragmentation::LastFragment,
            (true, true) => Fragmentation::Unfragmented,
        }
    }

    /// Get the transmission sequence number.
    pub fn transmission_sequence_number(&self) -> u32 {
        #[allow(clippy::indexing_slicing)]
        NetworkEndian::read_u32(&self.chunk.value()[0..=3])
    }

    /// Get the stream identifier.
    pub fn stream_identifier(&self) -> u16 {
        #[allow(clippy::indexing_slicing)]
        NetworkEndian::read_u16(&self.chunk.value()[4..=5])
    }

    /// Get the stream sequence number.
    pub fn stream_sequence_number(&self) -> u16 {
        #[allow(clippy::indexing_slicing)]
        NetworkEndian::read_u16(&self.chunk.value()[6..=7])
    }

    /// Get the payload protocol identifier.
    pub fn payload_protocol_ident(&self) -> u32 {
        #[allow(clippy::indexing_slicing)]
        NetworkEndian::read_u32(&self.chunk.value()[8..=11])
    }

    /// Get the contained user data.
    pub fn user_data(&self) -> &'bytes [u8] {
        #[allow(clippy::indexing_slicing)]
        &self.chunk.value()[FIXED_PARAMETER_SIZE as usize..]
    }
}

impl<'bytes> TryFrom<Chunk<'bytes>> for Data<'bytes> {
    type Error = GenericTryFromChunkError;

    fn try_from(chunk: Chunk<'bytes>) -> Result<Self, Self::Error> {
        chunk.verify_min_length(TYPE_ID, MINIMUM_SIZE)?;

        Ok(Self { chunk })
    }
}

impl<'bytes> From<Data<'bytes>> for Chunk<'bytes> {
    #[allow(clippy::cast_possible_truncation, clippy::integer_arithmetic)]
    fn from(data: Data<'bytes>) -> Self {
        data.chunk
    }
}

#[cfg(test)]
mod tests {
    use super::{Chunk, Data, Fragmentation, GenericTryFromChunkError, TryFrom, TYPE_ID};

    #[test]
    fn from_chunk() {
        let chunk = Chunk {
            bytes: &[
                TYPE_ID, 0x03, 0x00, 0x15, /* comment so rustfmt doesn't mess up alignment */
                0x7f, 0x99, 0x66, 0xc8, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                /* comment for rustfmt */
                0x74, 0x65, 0x73, 0x74, 0x0a,
            ],
        };

        let parsed_chunk = Data::try_from(chunk).expect("failed to parse chunk");

        assert_eq!(parsed_chunk.fragmentation(), Fragmentation::Unfragmented);
        assert_eq!(parsed_chunk.is_unordered(), false);
        assert_eq!(parsed_chunk.should_send_sack_immediately(), false);
        assert_eq!(parsed_chunk.transmission_sequence_number(), 2_140_759_752);
        assert_eq!(parsed_chunk.stream_identifier(), 0);
        assert_eq!(parsed_chunk.stream_sequence_number(), 0);
        assert_eq!(parsed_chunk.payload_protocol_ident(), 0);

        #[allow(clippy::wildcard_enum_match_arm)]
        let data_text = match std::str::from_utf8(parsed_chunk.user_data()) {
            Ok(text) => text,
            _ => panic!("Failed to parse user data as UTF-8"),
        };
        assert_eq!(data_text, "test\n");
    }

    #[test]
    fn from_chunk_with_invalid_length() {
        let chunk = Chunk {
            bytes: &[TYPE_ID, 0x03, 0x00, 0x04],
        };

        let err = Data::try_from(chunk).expect_err("expected BytesToChunkError::InvalidLength");
        assert_eq!(GenericTryFromChunkError::InvalidLength, err);
    }
}
