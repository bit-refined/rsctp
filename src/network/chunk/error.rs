use crate::network::generic_tlv::{ParsedTLV, TryTLVFromBytesError, HEADER_SIZE as HEADER_SIZE_TLV, TLV};

use super::{Chunk, GenericTryFromChunkError, TryFrom};

/// Type ID for this chunk.
pub const TYPE_ID: u8 = 9;
/// Minimum size of this chunk. Chunk header and at least the smallest possible error parameter.
const MINIMUM_SIZE: u16 = super::HEADER_SIZE + HEADER_SIZE_TLV;

/// Implements the `ERROR` chunk.
///
/// An endpoint sends this chunk to its peer endpoint to notify it of certain error conditions. It
/// contains one or more error causes. An Operation Error is not considered fatal in and of itself,
/// but may be used with an `ABORT` chunk to report a fatal condition.
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Error<'bytes> {
    /// The base chunk this specialized chunk is based upon.
    chunk: Chunk<'bytes>,
    /// One or more Error Causes defining the cause of the `ERROR`.
    error_causes: Vec<TLV<'bytes>>,
}

impl<'bytes> Error<'bytes> {
    /// Error causes contained in the `ERROR` chunk.
    pub const fn error_causes(&self) -> &Vec<TLV<'bytes>> {
        &self.error_causes
    }
}

/// Error returned by `TryFrom` for `Abort`.
#[derive(Debug)]
#[allow(variant_size_differences, clippy::module_name_repetitions)]
pub enum TryErrorFromChunkError<'bytes> {
    /// Verification error when trying to convert a generic TLV into a specialized one.
    ChunkError(GenericTryFromChunkError),
    /// Error when trying to convert bytes into a generic TLV.
    TLVParsing(TryTLVFromBytesError),
    /// Result is a valid `Abort` but we encountered parameters that require reporting.
    RequiredReporting(Error<'bytes>, Vec<TLV<'bytes>>),
}

impl<'bytes> TryFrom<Chunk<'bytes>> for Error<'bytes> {
    type Error = TryErrorFromChunkError<'bytes>;

    fn try_from(chunk: Chunk<'bytes>) -> Result<Self, Self::Error> {
        chunk.verify_min_length(TYPE_ID, MINIMUM_SIZE)?;

        let parsed_tlv = TLV::from_list(chunk.value(), crate::network::generic_tlv::error::is_recognized)?;

        match parsed_tlv {
            ParsedTLV::AllRecognized(error_causes)
            | ParsedTLV::SomeRecognized(error_causes)
            | ParsedTLV::Stop(error_causes) => Ok(Self { chunk, error_causes }),
            ParsedTLV::SomeRecognizedAndReport(error_causes, reported)
            | ParsedTLV::StopAndReport(error_causes, reported) => Err(TryErrorFromChunkError::RequiredReporting(
                Self { chunk, error_causes },
                reported,
            )),
        }
    }
}

impl<'bytes> From<Error<'bytes>> for Chunk<'bytes> {
    fn from(error: Error<'bytes>) -> Self {
        error.chunk
    }
}

impl From<GenericTryFromChunkError> for TryErrorFromChunkError<'_> {
    fn from(e: GenericTryFromChunkError) -> Self {
        TryErrorFromChunkError::ChunkError(e)
    }
}

impl From<TryTLVFromBytesError> for TryErrorFromChunkError<'_> {
    fn from(e: TryTLVFromBytesError) -> Self {
        TryErrorFromChunkError::TLVParsing(e)
    }
}

#[cfg(test)]
mod tests {
    use crate::network::generic_tlv::error::cookie_received_while_shutting_down::CookieReceivedWhileShuttingDown;
    use crate::network::generic_tlv::error::invalid_stream_identifier::InvalidStreamIdentifier;

    use super::{Chunk, Error, TryFrom, TYPE_ID};

    #[test]
    fn from_chunk() {
        let chunk = Chunk {
            bytes: &[
                TYPE_ID, 0x00, 0x00, 0x10,
                /* long comment that is needed so rustfmt doesn't mess up formatting */
                0x00, 0x01, 0x00, 0x08, 0x05, 0x39, 0x00, 0x00, // invalid stream identifier
                0x00, 0x0a, 0x00, 0x04, // cookie received while shutting down
            ],
        };

        let error = Error::try_from(chunk).unwrap();

        let mut causes = error.error_causes;

        let cause = causes.pop().unwrap();

        let _ = CookieReceivedWhileShuttingDown::try_from(cause).unwrap();

        let cause = causes.pop().unwrap();

        let e = InvalidStreamIdentifier::try_from(cause).unwrap();

        assert_eq!(1337, e.identifier());

        assert!(causes.is_empty());
    }
}
