use std::convert::TryFrom;
use std::num::NonZeroU16;

use byteorder::ByteOrder;
use byteorder::{LittleEndian, NetworkEndian};

use crc32c::{crc32c, crc32c_append};

use crate::network::chunk::abort::TryAbortFromChunkError;
use crate::network::chunk::error::TryErrorFromChunkError;
use crate::network::chunk::heartbeat::TryHeartbeatFromChunkError;
use crate::network::chunk::heartbeat_ack::TryHeartbeatACKFromChunkError;
use crate::network::chunk::init::TryInitFromChunkError;
use crate::network::chunk::init_ack::TryInitACKFromChunkError;
use crate::network::chunk::GenericTryFromChunkError;

use super::chunk::{BytesToChunkError, Chunk};

/// Packet header size
pub const HEADER_SIZE: usize = 12;

/// Error when parsing a `Packet` from bytes.
#[derive(Clone, Copy, Debug)]
pub enum BytesToPacketError {
    /// Packet is too short to be a valid packet.
    TooShort,
    /// The source port in the packet is zero.
    SourcePortZero,
    /// The destination port in the packet is zero.
    DestinationPortZero,
    /// The received checksum doesn't match the calculated checksum of the packet.
    BadChecksum,
    /// A protocol violation has been detected.
    ProtocolViolation,
    /// An error happened while parsing the chunk data. See [BytesToChunkError].
    ///
    /// [BytesToChunkError]: ../chunk/enum.BytesToChunkError.html
    ChunkError(BytesToChunkError),
}

/// Represent a single SCTP packet.
#[allow(dead_code)]
#[derive(Clone, Debug)]
pub struct Packet<'bytes> {
    /// The original bytes this packet was created from.
    bytes: &'bytes [u8],
    /// Source port of the packet.
    port_source: NonZeroU16,
    /// Destination port of the packet.
    port_destination: NonZeroU16,
    /// Verification Tag. The value of this Verification Tag MUST be set to the value of the
    /// Initiate Tag received from the peer endpoint during the association initialization.
    verification_tag: u32,
    /// CRC32c checksum of the entire packet with the checksum itself set to 0.
    checksum: u32,
    /// List of chunks contained in the packet.
    chunks: Vec<Chunk<'bytes>>,
}

impl<'bytes> Packet<'bytes> {
    /// Get this packet's source port. Guaranteed not to be zero.
    pub const fn source_port(&self) -> u16 {
        self.port_source.get()
    }

    /// Get this packet's destination port. Guaranteed not to be zero.
    pub const fn destination_port(&self) -> u16 {
        self.port_destination.get()
    }

    /// Get this packet's verification tag.
    pub const fn verification_tag(&self) -> u32 {
        self.verification_tag
    }

    /// Get this packet's checksum.
    pub const fn checksum(&self) -> u32 {
        self.checksum
    }

    /// Get this packet's chunks.
    pub const fn chunks(&self) -> &Vec<Chunk<'_>> {
        &self.chunks
    }

    /// Consume Packet and return all chunks within.
    #[allow(clippy::missing_const_for_fn)]
    pub fn into_chunks(self) -> Vec<Chunk<'bytes>> {
        self.chunks
    }
}

impl<'bytes> TryFrom<&'bytes [u8]> for Packet<'bytes> {
    type Error = BytesToPacketError;

    #[allow(clippy::indexing_slicing, clippy::integer_arithmetic)]
    fn try_from(bytes: &'bytes [u8]) -> Result<Self, Self::Error> {
        if bytes.len() < HEADER_SIZE {
            return Err(BytesToPacketError::TooShort);
        }

        // checksum calculation and comparison
        let calculated_checksum = calculate_checksum(bytes);
        let checksum = LittleEndian::read_u32(&bytes[8..=11]);

        if calculated_checksum != checksum {
            #[cfg(not(fuzzing))]
            return Err(BytesToPacketError::BadChecksum);
        }

        // port checks
        let port_source = match NonZeroU16::new(NetworkEndian::read_u16(&bytes[0..=1])) {
            Some(num) => num,
            None => return Err(BytesToPacketError::SourcePortZero),
        };

        let port_destination = match NonZeroU16::new(NetworkEndian::read_u16(&bytes[2..=3])) {
            Some(num) => num,
            None => return Err(BytesToPacketError::DestinationPortZero),
        };

        // verification tag
        let verification_tag = NetworkEndian::read_u32(&bytes[4..=7]);

        // chunk creation
        let mut chunks = Vec::new();
        let mut remaining_bytes = &bytes[HEADER_SIZE..];

        loop {
            // TODO: do NOT direct chunk errors "upwards" and instead handle them in here
            // this is mostly for the case that we find a unknown chunk, which still has to be
            // handled according to https://tools.ietf.org/html/rfc4960#section-3.2
            // another important error case is a bad length:
            // if a chunk says it has 5 bytes of value and we do not find exactly that many bytes,
            // it is a ProtocolViolation and has to be handled accordingly

            let new_chunk = Chunk::try_from(remaining_bytes).map_err(BytesToPacketError::ChunkError)?;
            //TODO: handle upcoming errors: unknown chunk -> https://tools.ietf.org/html/rfc4960#section-3.2

            if new_chunk.padded_length() > remaining_bytes.len() {
                // we received not enough bytes
                // possible reason: sender did not send enough padding
                return Err(BytesToPacketError::ProtocolViolation);
            }

            remaining_bytes = &remaining_bytes[new_chunk.padded_length()..];

            chunks.push(new_chunk);

            if remaining_bytes.is_empty() {
                break;
            }
        }

        let packet = Self {
            bytes,
            port_source,
            port_destination,
            verification_tag,
            checksum,
            chunks,
        };

        Ok(packet)
    }
}

/// Calcualte a `CRC32c` checksum in little-endian.
///
/// # Panics
///
/// This function will panic, if the given byte slice has less than 13 elements. A length check
/// should be done by the caller.
#[allow(dead_code, clippy::indexing_slicing)]
fn calculate_checksum(bytes: &[u8]) -> u32 {
    // calculate the checksum until we reach it in the header
    let mut crc = crc32c(&bytes[0..=7]);

    // set the checksum to 0
    crc = crc32c_append(crc, &[0, 0, 0, 0]);

    // append the rest
    crc32c_append(crc, &bytes[12..])
}

/// All errors that represent a failure during fuzzing.
pub enum FuzzError<'bytes> {
    /// `ABORT` Chunk
    Abort(TryAbortFromChunkError<'bytes>),
    /// `ERROR` Chunk
    Error(TryErrorFromChunkError<'bytes>),
    /// non-specific chunks mostly the ones without parameters
    Generic(GenericTryFromChunkError),
    /// `HEARTBEAT` Chunk
    Heartbeat(TryHeartbeatFromChunkError<'bytes>),
    /// `HEARTBEAT ACK` Chunk
    HeartbeatACK(TryHeartbeatACKFromChunkError<'bytes>),
    /// `INIT` Chunk
    Init(TryInitFromChunkError<'bytes>),
    /// `INIT ACK` Chunk
    InitACK(TryInitACKFromChunkError<'bytes>),
}

impl<'bytes> From<TryAbortFromChunkError<'bytes>> for FuzzError<'bytes> {
    fn from(e: TryAbortFromChunkError<'bytes>) -> Self {
        FuzzError::Abort(e)
    }
}

impl<'bytes> From<TryErrorFromChunkError<'bytes>> for FuzzError<'bytes> {
    fn from(e: TryErrorFromChunkError<'bytes>) -> Self {
        FuzzError::Error(e)
    }
}

impl From<GenericTryFromChunkError> for FuzzError<'_> {
    fn from(e: GenericTryFromChunkError) -> Self {
        FuzzError::Generic(e)
    }
}

impl<'bytes> From<TryHeartbeatFromChunkError<'bytes>> for FuzzError<'bytes> {
    fn from(e: TryHeartbeatFromChunkError<'bytes>) -> Self {
        FuzzError::Heartbeat(e)
    }
}

impl<'bytes> From<TryHeartbeatACKFromChunkError<'bytes>> for FuzzError<'bytes> {
    fn from(e: TryHeartbeatACKFromChunkError<'bytes>) -> Self {
        FuzzError::HeartbeatACK(e)
    }
}

impl<'bytes> From<TryInitFromChunkError<'bytes>> for FuzzError<'bytes> {
    fn from(e: TryInitFromChunkError<'bytes>) -> Self {
        FuzzError::Init(e)
    }
}

impl<'bytes> From<TryInitACKFromChunkError<'bytes>> for FuzzError<'bytes> {
    fn from(e: TryInitACKFromChunkError<'bytes>) -> Self {
        FuzzError::InitACK(e)
    }
}

#[cfg(test)]
mod tests {
    use super::calculate_checksum;

    #[test]
    fn checksum() {
        let bytes = &[
            0x30, 0x39, 0x30, 0x39, 0x88, 0x42, 0x3a, 0xbf, 0xce, 0xe6, 0x6a, 0x2a, 0x0e, 0x00, 0x00, 0x04,
        ];

        assert_eq!(u32::from_le_bytes([0xce, 0xe6, 0x6a, 0x2a]), calculate_checksum(bytes));
    }

    //    #[test]
    //    fn bytes_to_packet() {
    //        let bytes = [
    //            0x30, 0x39, 0x30, 0x39, 0x88, 0x42, 0x3a, 0xbf, 0xce, 0xe6, 0x6a, 0x2a, 0x0e, 0x00, 0x00, 0x04,
    //        ];
    //
    //        let expected_packet = Packet {
    //            port_source: NonZeroU16::new(12345).expect("This can't fail"),
    //            port_destination: NonZeroU16::new(12345).expect("This can't fail"),
    //            verification_tag: 0x88_42_3a_bf,
    //            checksum: u32::from_le_bytes([0xce, 0xe6, 0x6a, 0x2a]),
    //            chunks: vec![Chunks::ShutdownComplete(ShutdownComplete {
    //                flag_reflected_verification_tag: false,
    //            })],
    //        };
    //
    //        let actual_packet = Packet::try_from(&bytes[..]).expect("failed to parse packet");
    //
    //        assert_eq!(expected_packet.source_port(), actual_packet.source_port());
    //        assert_eq!(expected_packet.destination_port(), actual_packet.destination_port());
    //        assert_eq!(expected_packet.verification_tag(), actual_packet.verification_tag());
    //        assert_eq!(expected_packet.checksum(), actual_packet.checksum());
    //        assert_eq!(expected_packet.chunks(), actual_packet.chunks());
    //    }
}
