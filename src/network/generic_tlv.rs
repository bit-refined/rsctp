use std::convert::TryFrom;

use byteorder::{ByteOrder, NetworkEndian};

use crate::network::padding::total_padded_length;

/// Errors for the `ERROR` and `ABORT` chunk.
pub mod error;
/// Parameters for all chunks.
pub mod parameter;

/// TLV header size
pub const HEADER_SIZE: u16 = 4;

/// Bitmask for the action of skipping an unknown parameter.
const BITMASK_SKIP: u16 = 0b1000_0000_0000_0000;
/// Bitmask for the action of reporting an unknown parameter.
const BITMASK_REPORT: u16 = 0b0100_0000_0000_0000;

/// Chunk values of SCTP control chunks consist of a chunk-type-specific
/// header of required fields, followed by zero or more parameters.
/// The optional and variable-length parameters contained in a chunk are
/// defined in a Type-Length-Value format as shown below.
///```plaintext
///      0                   1                   2                   3
///      0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
///     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
///     |          Parameter Type       |       Parameter Length        |
///     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
///     \                                                               \
///     /                       Parameter Value                         /
///     \                                                               \
///     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
/// ```
#[derive(Clone, Debug, Ord, PartialOrd, Eq, PartialEq)]
pub struct TLV<'bytes> {
    /// This field contains the exact slice of bytes given by the packet in
    /// which it is located.
    pub bytes: &'bytes [u8],
}

impl<'bytes> TLV<'bytes> {
    /// The Type field is a 16-bit identifier of the type of parameter.
    /// It takes a value of 0 to 65534.
    ///
    /// The value of 65535 is reserved for IETF-defined extensions.
    /// Values other than those defined in specific SCTP chunk
    /// descriptions are reserved for use by IETF.
    pub fn ty(&self) -> u16 {
        #[allow(clippy::indexing_slicing)]
        NetworkEndian::read_u16(&self.bytes[0..=1])
    }

    /// The Parameter Length field contains the size of the parameter in
    /// bytes, including the Parameter Type, Parameter Length, and
    /// Parameter Value fields.
    /// Thus, a parameter with a zero-length Parameter Value field would have a Length field of 4.
    ///
    /// The Parameter Length does not include any padding bytes.
    pub fn length(&self) -> u16 {
        #[allow(clippy::indexing_slicing)]
        NetworkEndian::read_u16(&self.bytes[2..=3])
    }

    /// This value represents the size of the parameter in bytes, including
    /// the Parameter Type, Parameter Length, and Parameter Value fields
    /// with the addition of required padding.
    pub fn padded_length(&self) -> usize {
        total_padded_length(self.length())
    }

    /// The Parameter Value field contains the actual information to be
    /// transferred in the parameter.
    pub fn value(&self) -> &'bytes [u8] {
        #[allow(clippy::indexing_slicing)]
        &self.bytes[HEADER_SIZE as usize..]
    }

    /// Helper function to verify that this chunk has the specified type and
    /// minimum length. Intended for usage in `TryFrom<Chunk> for C` impls
    /// where `C` is one of the concrete chunk types.
    pub fn verify_min_length(&self, ty: u16, len: u16) -> Result<(), GenericTryFromTLVError> {
        if self.ty() != ty {
            Err(GenericTryFromTLVError::InvalidType)?;
        }

        if self.length() < len {
            Err(GenericTryFromTLVError::InvalidLength)?;
        }

        Ok(())
    }

    /// Helper function to verify that this chunk has the specified type and
    /// exact length. Intended for usage in `TryFrom<Chunk> for C` impls where
    /// `C` is one of the concrete chunk types.
    pub fn verify_static_length(&self, ty: u16, len: u16) -> Result<(), GenericTryFromTLVError> {
        if self.ty() != ty {
            Err(GenericTryFromTLVError::InvalidType)?;
        }

        if self.length() != len {
            Err(GenericTryFromTLVError::InvalidLength)?;
        }

        Ok(())
    }

    /// If true, skip this parameter and continue processing. Otherwise, stop processing this
    /// parameter; do not process any further parameters within a chunk.
    pub fn skip(&self) -> bool {
        self.ty() & BITMASK_SKIP != 0
    }

    /// If true, report the unrecognized parameter as described in [RFC 4960 § 3.2.2].
    ///
    /// [RFC 4960 § 3.2.2]: https://tools.ietf.org/html/rfc4960#section-3.2.2
    pub fn report(&self) -> bool {
        self.ty() & BITMASK_REPORT != 0
    }

    /// The method processes parameters according to [RFC 4960 § 3.2.1] and will stop or skip in
    /// case an unrecognized parameter is found.
    ///
    /// This function can fail, but only if there is a problem when creating a TLV
    /// from the given data. This either means there's not enough or garbage data.
    ///
    /// [RFC 4960 § 3.2.1]: https://tools.ietf.org/html/rfc4960#section-3.2.1
    pub fn from_list<F>(mut bytes: &[u8], recognized: F) -> Result<ParsedTLV<'_>, TryTLVFromBytesError>
    where
        F: Fn(u16) -> bool,
    {
        let mut params = Vec::new();
        let mut reported_params = Vec::new();
        let mut ignored = false;
        let mut stopped = false;

        #[allow(clippy::indexing_slicing)]
        while !bytes.is_empty() {
            let tlv = TLV::try_from(bytes)?;

            // prepare byte slice for next iteration
            // this `.min()` is here because it might happen, that there are no padding bytes in
            // tests or when running hfuzz
            bytes = &bytes[bytes.len().min(tlv.padded_length())..];

            if recognized(tlv.ty()) {
                params.push(tlv)
            } else {
                if !tlv.skip() {
                    // this is necessary so we can move `tlv` before we return
                    stopped = true;
                }

                if tlv.report() {
                    reported_params.push(tlv)
                } else {
                    ignored = true;
                }

                if stopped {
                    if reported_params.is_empty() {
                        return Ok(ParsedTLV::Stop(params));
                    } else {
                        return Ok(ParsedTLV::StopAndReport(params, reported_params));
                    }
                }
            }
        }

        if reported_params.is_empty() {
            if ignored {
                Ok(ParsedTLV::SomeRecognized(params))
            } else {
                Ok(ParsedTLV::AllRecognized(params))
            }
        } else {
            Ok(ParsedTLV::SomeRecognizedAndReport(params, reported_params))
        }
    }
}

/// Result of parsing a list of TLVs.
pub enum ParsedTLV<'bytes> {
    /// All parameters were parsed successfully.
    AllRecognized(Vec<TLV<'bytes>>),
    /// Some parameters were parsed but we ignore some unrecognized ones.
    SomeRecognized(Vec<TLV<'bytes>>),
    /// Some parameters were parsed and there are unrecognized ones that require reporting.
    SomeRecognizedAndReport(Vec<TLV<'bytes>>, Vec<TLV<'bytes>>),
    /// The last parameter is unrecognized and its type ID requires a parsing stop.
    /// There are no unrecognized parameters that require reporting.
    Stop(Vec<TLV<'bytes>>),
    /// The last parameter is unrecognized and its type ID requires a parsing stop.
    /// Additionally, there are unrecognized parameters that require reporting.
    StopAndReport(Vec<TLV<'bytes>>, Vec<TLV<'bytes>>),
}

impl<'bytes> TryFrom<&'bytes [u8]> for TLV<'bytes> {
    type Error = TryTLVFromBytesError;

    #[allow(clippy::shadow_reuse)]
    fn try_from(bytes: &'bytes [u8]) -> Result<Self, Self::Error> {
        if bytes.len() < HEADER_SIZE as usize {
            return Err(TryTLVFromBytesError::TooShort);
        }

        #[allow(clippy::indexing_slicing)]
        let length = NetworkEndian::read_u16(&bytes[2..=3]);

        // because the length includes the header it has to be at least that size
        if length < HEADER_SIZE {
            return Err(TryTLVFromBytesError::InvalidLength);
        }

        // the given slice does not contain enough bytes to correctly construct
        // a parameter or error with the given length
        if bytes.len() < length as usize {
            return Err(TryTLVFromBytesError::InvalidLength);
        }

        // we only save a reference to the part that interests us and automatically ignore padding
        #[allow(clippy::indexing_slicing)]
        let bytes = &bytes[..length as usize];

        Ok(TLV { bytes })
    }
}

/// Error when parsing `TLV` from bytes.
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
#[allow(variant_size_differences)]
pub enum TryTLVFromBytesError {
    /// TLV-parameter is too short to be valid.
    TooShort,
    /// TLV-parameter has a invalid length, indicating a [ProtocolViolation].
    ///
    /// [ProtocolViolation]: error/protocol_violation/struct.ProtocolViolation.html
    InvalidLength,
}

/// Verification error when trying to convert a generic TLV into a specialized one.
#[derive(Debug, Clone, Copy)]
pub enum GenericTryFromTLVError {
    /// The conversion from a generic TLV-parameter to a specialized one failed because the type
    /// bytes do not match the required value.
    InvalidType,
    /// TLV-parameter has a invalid length, indicating a [ProtocolViolation].
    ///
    /// [ProtocolViolation]: error/protocol_violation/struct.ProtocolViolation.html
    InvalidLength,
}

/// Allows to transparently work with any address type.
#[derive(Debug, PartialEq, Eq)]
pub enum AddrType<'bytes> {
    /// IPv4 parameter
    IPv4(parameter::IPv4<'bytes>),
    /// IPv6 parameter
    IPv6(parameter::IPv6<'bytes>),
    /// Hostname parameter
    HostNameAddress(parameter::HostNameAddress<'bytes>),
}

// TODO: Test for TLV::from_list()!
