use std::convert::TryFrom;

use byteorder::{ByteOrder, NetworkEndian};

use crate::network::generic_tlv::{GenericTryFromTLVError, TLV};

/// Type ID for this parameter.
pub const TYPE_ID: u16 = 9;
/// Length of this parameter. Must always be 8.
const STATIC_SIZE: u16 = super::HEADER_SIZE + 4;

/// Implements the `Cookie Preservative` parameter. See also [RFC 4960 § 3.3.2.1].
///
/// The sender of an `INIT` shall use this parameter to suggest to the receiver of the `INIT` for a
/// longer life-span of the State Cookie.
///
/// [RFC 4960 § 3.3.2.1]: https://tools.ietf.org/html/rfc4960#section-3.3.2.1
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct CookiePreservative<'bytes> {
    /// The base TLV-parameter this specialized parameter is based upon.
    tlv: TLV<'bytes>,
}

impl<'bytes> CookiePreservative<'bytes> {
    /// This parameter indicates to the receiver how much increment in milliseconds the sender
    /// wishes the receiver to add to its default cookie life-span.
    pub fn suggested_cookie_lifespan_increment(&self) -> u32 {
        #[allow(clippy::indexing_slicing)]
        NetworkEndian::read_u32(&self.tlv.bytes[4..=7])
    }
}

impl<'bytes> TryFrom<TLV<'bytes>> for CookiePreservative<'bytes> {
    type Error = GenericTryFromTLVError;

    fn try_from(tlv: TLV<'bytes>) -> Result<Self, Self::Error> {
        tlv.verify_static_length(TYPE_ID, STATIC_SIZE)?;

        Ok(Self { tlv })
    }
}

impl<'bytes> From<CookiePreservative<'bytes>> for TLV<'bytes> {
    fn from(cookie_preservative: CookiePreservative<'bytes>) -> Self {
        cookie_preservative.tlv
    }
}

#[cfg(test)]
mod tests {
    use std::convert::TryFrom;

    use super::{CookiePreservative, STATIC_SIZE, TLV, TYPE_ID};

    #[test]
    fn from_tlv() {
        let tlv = TLV::try_from(&[0, 9, 0, 8, 0, 0, 5, 57][..]).unwrap();

        assert_eq!(tlv.ty(), TYPE_ID);
        assert_eq!(tlv.length(), STATIC_SIZE);

        let e = CookiePreservative::try_from(tlv).unwrap();

        assert_eq!(e.suggested_cookie_lifespan_increment(), 1337);
    }
}
