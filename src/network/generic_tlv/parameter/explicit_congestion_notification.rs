use std::convert::TryFrom;

use crate::network::generic_tlv::{GenericTryFromTLVError, TLV};

// RFC4960 specifies 32768 directly, so use that here as well
#[allow(clippy::decimal_literal_representation)]
/// Type ID for this parameter.
pub const TYPE_ID: u16 = 32768;
/// Length of this parameter. Must always be 4.
const STATIC_SIZE: u16 = super::HEADER_SIZE;

/// Implements the `Explicit Congestion Notification` parameter. See also [RFC 4960 Appendix A].
///
/// To indicate that an endpoint is ECN capable, an endpoint SHOULD add this parameter to the
/// `INIT` and or `INIT_ACK` chunk.
///
/// [RFC 4960 Appendix A]: https://tools.ietf.org/html/rfc4960#appendix-A
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct ExplicitCongestionNotification<'bytes> {
    /// The base TLV-parameter this specialized parameter is based upon.
    tlv: TLV<'bytes>,
}

impl<'bytes> TryFrom<TLV<'bytes>> for ExplicitCongestionNotification<'bytes> {
    type Error = GenericTryFromTLVError;

    fn try_from(tlv: TLV<'bytes>) -> Result<Self, Self::Error> {
        tlv.verify_static_length(TYPE_ID, STATIC_SIZE)?;

        Ok(Self { tlv })
    }
}

impl<'bytes> From<ExplicitCongestionNotification<'bytes>> for TLV<'bytes> {
    fn from(explicit_congestion_notification: ExplicitCongestionNotification<'bytes>) -> Self {
        explicit_congestion_notification.tlv
    }
}

#[cfg(test)]
mod tests {
    use std::convert::TryFrom;

    use super::{ExplicitCongestionNotification, STATIC_SIZE, TLV, TYPE_ID};

    #[test]
    fn from_tlv() {
        let tlv = TLV::try_from(&[128, 0, 0, 4][..]).unwrap();

        assert_eq!(tlv.ty(), TYPE_ID);
        assert_eq!(tlv.length(), STATIC_SIZE);

        let _ = ExplicitCongestionNotification::try_from(tlv).unwrap();
    }
}
