use std::convert::TryFrom;

use crate::network::generic_tlv::{GenericTryFromTLVError, TLV};

/// Type ID for this parameter.
pub const TYPE_ID: u16 = 1;
/// Minimum length of this parameter. 4 bytes header.
const MINIMUM_SIZE: u16 = super::HEADER_SIZE;

/// Implements the `Heartbeat Info` parameter. See also [RFC 4960 § 3.3.5].
///
/// The Sender-Specific Heartbeat Info field contains an opaque data structure only understood by
/// the sender of a `HEARTBEAT` chunk and  should normally include information about the sender's
/// current time when a `HEARTBEAT` chunk is sent and the destination transport address to which
/// that `HEARTBEAT` is sent.  This information is simply reflected back by the receiver in the
/// `HEARTBEAT_ACK` message.  Note also that the `HEARTBEAT` message is both for reachability
/// checking and for path verification. When a `HEARTBEAT` chunk is being used for path
/// verification purposes, it MUST hold a 64-bit random nonce.
///
/// [RFC 4960 § 3.3.5]: https://tools.ietf.org/html/rfc4960#section-3.3.5
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct HeartbeatInfo<'bytes> {
    /// The base TLV-parameter this specialized parameter is based upon.
    tlv: TLV<'bytes>,
}

impl<'bytes> HeartbeatInfo<'bytes> {
    /// Contains an opaque data structure only understood by the sender of a `HEARBEAT` chunk.
    pub fn sender_specific_heartbeat_info(&self) -> &'bytes [u8] {
        self.tlv.value()
    }
}

impl<'bytes> TryFrom<TLV<'bytes>> for HeartbeatInfo<'bytes> {
    type Error = GenericTryFromTLVError;

    fn try_from(tlv: TLV<'bytes>) -> Result<Self, Self::Error> {
        tlv.verify_min_length(TYPE_ID, MINIMUM_SIZE)?;

        Ok(Self { tlv })
    }
}

impl<'bytes> From<HeartbeatInfo<'bytes>> for TLV<'bytes> {
    fn from(heartbeat_info: HeartbeatInfo<'bytes>) -> Self {
        heartbeat_info.tlv
    }
}

#[cfg(test)]
mod tests {
    use std::convert::TryFrom;

    use super::{HeartbeatInfo, MINIMUM_SIZE, TLV, TYPE_ID};

    #[test]
    fn from_tlv() {
        let tlv = TLV::try_from(&[0, 1, 0, 6, 42, 24][..]).unwrap();

        assert_eq!(tlv.ty(), TYPE_ID);
        assert!(tlv.length() >= MINIMUM_SIZE);
        assert_eq!(tlv.length(), 6);

        let e = HeartbeatInfo::try_from(tlv).unwrap();

        assert_eq!(e.sender_specific_heartbeat_info().to_vec(), vec![42, 24]);
    }
}
