use std::convert::TryFrom;

use crate::network::generic_tlv::{GenericTryFromTLVError, TryTLVFromBytesError, TLV};

/// Type ID for this parameter.
pub const TYPE_ID: u16 = 8;
/// Minimum size for this parameter. 4 bytes header + at least one unknown parameter with at least 4 bytes header
const MINIMUM_SIZE: u16 = 8;

/// Implements the `Unrecognized Parameter` parameter. See also [RFC 4960 § 3.3.3.1].
///
/// This parameter is returned to the originator of the `INIT` chunk when the `INIT` contains an
/// unrecognized parameter that has a value that indicates it should be reported to the sender.
/// This parameter value field will contain unrecognized parameters copied from the `INIT` chunk
/// complete with `Parameter Type`, `Length`, and `Value` fields.
///
/// [RFC 4960 § 3.3.3.1]: https://tools.ietf.org/html/rfc4960#section-3.3.3.1
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct UnrecognizedParameters<'bytes> {
    /// The base TLV-parameter this specialized parameter is based upon.
    tlv: TLV<'bytes>,
    parameters: Vec<TLV<'bytes>>,
}

impl<'bytes> UnrecognizedParameters<'bytes> {
    /// The Unrecognized Parameters field contains the unrecognized parameters copied from
    /// the INIT ACK chunk complete with TLV. This error cause is normally contained in
    /// an ERROR chunk bundled with the COOKIE ECHO chunk when responding to the INIT ACK, when the
    /// sender of the COOKIE ECHO chunk wishes to report unrecognized parameters.
    pub const fn unrecognized_parameters(&self) -> &Vec<TLV<'_>> {
        &self.parameters
    }
}

/// Error returned by `TryFrom` for `UnrecognizedParameter`.
#[derive(Copy, Clone, Debug)]
pub enum TryUnrecognizedParametersFromTLVError {
    /// Verification error when trying to convert a generic TLV into a specialized one.
    Generic(GenericTryFromTLVError),
    /// Error when trying to convert bytes into a generic TLV.
    TLVParsing(TryTLVFromBytesError),
}

impl<'bytes> TryFrom<TLV<'bytes>> for UnrecognizedParameters<'bytes> {
    type Error = TryUnrecognizedParametersFromTLVError;

    fn try_from(tlv: TLV<'bytes>) -> Result<Self, Self::Error> {
        tlv.verify_min_length(TYPE_ID, MINIMUM_SIZE)?;

        let mut bytes = tlv.value();
        let mut parameters = Vec::new();

        // This is basically `TLV::from_list()` but without sorting and ignoring.
        #[allow(clippy::indexing_slicing)]
        while !bytes.is_empty() {
            let inner_tlv = TLV::try_from(bytes)?;

            // prepare byte slice for next iteration
            // this `.min()` is here because it might happen, that there are no padding bytes in
            // tests or when running hfuzz
            bytes = &bytes[bytes.len().min(inner_tlv.padded_length())..];

            parameters.push(inner_tlv);
        }

        Ok(Self { tlv, parameters })
    }
}

impl<'bytes> From<UnrecognizedParameters<'bytes>> for TLV<'bytes> {
    fn from(unrecognized_parameter: UnrecognizedParameters<'bytes>) -> Self {
        unrecognized_parameter.tlv
    }
}

impl From<GenericTryFromTLVError> for TryUnrecognizedParametersFromTLVError {
    fn from(e: GenericTryFromTLVError) -> Self {
        TryUnrecognizedParametersFromTLVError::Generic(e)
    }
}

impl From<TryTLVFromBytesError> for TryUnrecognizedParametersFromTLVError {
    fn from(e: TryTLVFromBytesError) -> Self {
        TryUnrecognizedParametersFromTLVError::TLVParsing(e)
    }
}

#[cfg(test)]
mod tests {
    use std::convert::TryFrom;

    use super::{UnrecognizedParameters, MINIMUM_SIZE, TLV, TYPE_ID};

    #[test]
    fn from_tlv() {
        let tlv = TLV::try_from(&[0, 8, 0, 12, 0, 127, 0, 4, 0, 128, 0, 4][..]).unwrap();

        assert_eq!(tlv.ty(), TYPE_ID);
        assert!(tlv.length() >= MINIMUM_SIZE);
        assert_eq!(tlv.length(), 12);

        let p = UnrecognizedParameters::try_from(tlv).unwrap();

        assert_eq!(
            &vec![
                TLV::try_from(&[0, 127, 0, 4][..]).unwrap(),
                TLV::try_from(&[0, 128, 0, 4][..]).unwrap()
            ],
            p.unrecognized_parameters()
        );
    }
}
