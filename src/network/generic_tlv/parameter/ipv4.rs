use std::convert::TryFrom;
use std::net::Ipv4Addr;

use crate::network::generic_tlv::{GenericTryFromTLVError, TLV};

/// Type ID for this parameter.
pub const TYPE_ID: u16 = 5;
/// Length of this parameter. Must always be 8.
const STATIC_SIZE: u16 = super::HEADER_SIZE + 4;

/// Implements the `IPv4` parameter. See also [RFC 4960 § 3.3.2.1].
///
/// This parameter contains an IPv4 address of the sending endpoint. It is binary encoded.
///
/// [RFC 4960 § 3.3.2.1]: https://tools.ietf.org/html/rfc4960#section-3.3.2.1
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct IPv4<'bytes> {
    /// The base TLV-parameter this specialized parameter is based upon.
    tlv: TLV<'bytes>,
}

impl IPv4<'_> {
    /// Returns the IP contained in this parameter in form of an [Ipv4Addr]
    ///
    /// [Ipv4Addr]: https://doc.rust-lang.org/std/net/struct.Ipv4Addr.html
    #[allow(clippy::indexing_slicing)]
    pub fn ip(&self) -> Ipv4Addr {
        let bytes = self.tlv.value();
        Ipv4Addr::new(bytes[0], bytes[1], bytes[2], bytes[3])
    }
}

impl<'bytes> TryFrom<TLV<'bytes>> for IPv4<'bytes> {
    type Error = GenericTryFromTLVError;

    fn try_from(tlv: TLV<'bytes>) -> Result<Self, Self::Error> {
        tlv.verify_static_length(TYPE_ID, STATIC_SIZE)?;

        Ok(Self { tlv })
    }
}

impl<'bytes> From<IPv4<'bytes>> for TLV<'bytes> {
    fn from(ipv4: IPv4<'bytes>) -> Self {
        ipv4.tlv
    }
}

#[cfg(test)]
mod tests {
    use std::convert::TryFrom;

    use super::{IPv4, Ipv4Addr, STATIC_SIZE, TLV, TYPE_ID};

    #[test]
    fn from_tlv() {
        let tlv = TLV::try_from(&[0, 5, 0, 8, 172, 16, 0, 1][..]).unwrap();

        assert_eq!(tlv.ty(), TYPE_ID);
        assert_eq!(tlv.length(), STATIC_SIZE);

        let p = IPv4::try_from(tlv).unwrap();

        assert_eq!(Ipv4Addr::new(172, 16, 0, 1), p.ip())
    }
}
