pub(crate) use self::{
    cookie_preservative::CookiePreservative, explicit_congestion_notification::ExplicitCongestionNotification,
    heartbeat_info::HeartbeatInfo, host_name_address::HostNameAddress, ipv4::IPv4, ipv6::IPv6,
    state_cookie::StateCookie, supported_address_types::SupportedAddressTypes,
    unrecognized_parameters::UnrecognizedParameters,
};

/// Cookie Preservative parameter.
pub mod cookie_preservative;
/// Explicit Congestion Notification parameter.
pub mod explicit_congestion_notification;
/// Heartbeat Info parameter.
pub mod heartbeat_info;
/// Host Name Address parameter.
pub mod host_name_address;
/// IPv4 parameter.
pub mod ipv4;
/// IPv6 parameter.
pub mod ipv6;
/// State Cookie parameter.
pub mod state_cookie;
/// Supported Address Types parameter.
pub mod supported_address_types;
/// Unrecognized Parameter parameter.
#[allow(clippy::module_name_repetitions)]
pub mod unrecognized_parameters;

/// Error when parsing `Parameter` from bytes.
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum BytesToParameterError {
    /// Parameter is too short to be a valid parameter.
    TooShort,
    /// Parameter has a invalid length, indicating a [ProtocolViolation].
    ///
    /// [ProtocolViolation]: ../error/protocol_violation/struct.ProtocolViolation.html
    InvalidLength,
    /// Error in the parameter data.
    DataError,
}

/// Parameter header size
pub const HEADER_SIZE: u16 = 4;

/// Returns true if the given `parameter_type` has a value
/// with a corresponding RSCTP parameter `TYPE_ID`.
pub fn is_recognized(parameter_type: u16) -> bool {
    match parameter_type {
        cookie_preservative::TYPE_ID
        | explicit_congestion_notification::TYPE_ID
        | heartbeat_info::TYPE_ID
        | host_name_address::TYPE_ID
        | ipv4::TYPE_ID
        | ipv6::TYPE_ID
        | state_cookie::TYPE_ID
        | supported_address_types::TYPE_ID
        | unrecognized_parameters::TYPE_ID => true,
        _ => false,
    }
}
