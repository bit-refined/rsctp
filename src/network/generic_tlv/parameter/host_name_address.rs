use std::convert::TryFrom;
use std::str::Utf8Error;

use crate::network::generic_tlv::{GenericTryFromTLVError, TLV};

/// Type ID for this parameter.
pub const TYPE_ID: u16 = 11;
/// Minimum length of this parameter. 4 bytes header, at least 1 byte null terminator in Host Name.
const MINIMUM_SIZE: u16 = 5;

/// Implements the `Host Name Address` parameter. See also [RFC 4960 § 3.3.2.1].
///
/// The sender of an `INIT` uses this parameter to pass its Host Name (in place of its IP
/// addresses) to its peer. The peer is responsible for resolving the name. Using this parameter
/// might make it more likely for the association to work across a NAT box.
///
/// [RFC 4960 § 3.3.2.1]: https://tools.ietf.org/html/rfc4960#section-3.3.2.1
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct HostNameAddress<'bytes> {
    /// The base TLV-parameter this specialized parameter is based upon.
    tlv: TLV<'bytes>,
    /// This field contains a host name in "host name syntax" per [RFC 1123 § 2.1]. At least one
    /// null terminator is included in the Host Name string and must be included in the length.
    ///
    /// [RFC 1123 § 2.1]: https://tools.ietf.org/html/rfc1123#section-2.1
    host_name: &'bytes str,
}

/// Error returned by `TryFrom` for `HostNameAddress`.
#[derive(Clone, Copy, Debug)]
#[allow(variant_size_differences)]
pub enum TryHostNameAddressFromTLVError {
    /// Verification error when trying to convert a generic TLV into a specialized one.
    Generic(GenericTryFromTLVError),
    /// Contained `HostName` is not valid UTF8.
    Utf8Error(Utf8Error),
}

impl<'bytes> TryFrom<TLV<'bytes>> for HostNameAddress<'bytes> {
    type Error = TryHostNameAddressFromTLVError;

    fn try_from(tlv: TLV<'bytes>) -> Result<Self, Self::Error> {
        tlv.verify_min_length(TYPE_ID, MINIMUM_SIZE)?;

        let host_name = std::str::from_utf8(tlv.value())?;

        Ok(Self { tlv, host_name })
    }
}

impl<'bytes> From<HostNameAddress<'bytes>> for TLV<'bytes> {
    fn from(host_name_address: HostNameAddress<'bytes>) -> Self {
        host_name_address.tlv
    }
}

impl From<GenericTryFromTLVError> for TryHostNameAddressFromTLVError {
    fn from(e: GenericTryFromTLVError) -> Self {
        TryHostNameAddressFromTLVError::Generic(e)
    }
}

impl From<Utf8Error> for TryHostNameAddressFromTLVError {
    fn from(e: Utf8Error) -> Self {
        TryHostNameAddressFromTLVError::Utf8Error(e)
    }
}

#[cfg(test)]
mod tests {
    use std::convert::TryFrom;

    use super::{HostNameAddress, MINIMUM_SIZE, TLV, TYPE_ID};

    #[test]
    fn from_tlv() {
        let tlv = TLV::try_from(&[0, 11, 0, 9, 0x74, 0x65, 0x73, 0x74, 0x0a][..]).unwrap();

        assert_eq!(tlv.ty(), TYPE_ID);
        assert!(tlv.length() >= MINIMUM_SIZE);
        assert_eq!(tlv.length(), 9);

        let e = HostNameAddress::try_from(tlv).unwrap();

        assert_eq!(e.host_name, "test\n");
    }
}
