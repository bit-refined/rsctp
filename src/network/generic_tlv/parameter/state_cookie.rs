use std::convert::TryFrom;

use crate::network::generic_tlv::{GenericTryFromTLVError, TLV};

/// Type ID for this parameter.
pub const TYPE_ID: u16 = 7;
/// Minimum length of this parameter. 4 bytes header, no data.
const MINIMUM_SIZE: u16 = super::HEADER_SIZE;

/// Implements the `State Cookie` parameter. See also [RFC 4960 § 3.3.3.1].
///
/// This parameter value MUST contain all the necessary state and parameter information required
/// for the sender of this `INIT_ACK` to create the association, along with a Message
/// Authentication Code (MAC).
///
/// [RFC 4960 § 3.3.3.1]: https://tools.ietf.org/html/rfc4960#section-3.3.3.1
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct StateCookie<'bytes> {
    /// The base TLV-parameter this specialized parameter is based upon.
    tlv: TLV<'bytes>,
}

impl<'bytes> StateCookie<'bytes> {
    /// Data of the State Cookie parameter.
    pub fn cookie(&self) -> &'bytes [u8] {
        self.tlv.value()
    }
}

impl<'bytes> TryFrom<TLV<'bytes>> for StateCookie<'bytes> {
    type Error = GenericTryFromTLVError;

    fn try_from(tlv: TLV<'bytes>) -> Result<Self, Self::Error> {
        tlv.verify_min_length(TYPE_ID, MINIMUM_SIZE)?;

        Ok(Self { tlv })
    }
}

impl<'bytes> From<StateCookie<'bytes>> for TLV<'bytes> {
    fn from(state_cookie: StateCookie<'bytes>) -> Self {
        state_cookie.tlv
    }
}

#[cfg(test)]
mod tests {
    use std::convert::TryFrom;

    use super::{StateCookie, MINIMUM_SIZE, TLV, TYPE_ID};

    #[test]
    fn from_tlv() {
        let tlv = TLV::try_from(&[0, 7, 0, 6, 42, 24][..]).unwrap();

        assert_eq!(tlv.ty(), TYPE_ID);
        assert!(tlv.length() >= MINIMUM_SIZE);
        assert_eq!(tlv.length(), 6);

        let e = StateCookie::try_from(tlv).unwrap();

        assert_eq!(e.cookie(), &[42, 24]);
    }
}
