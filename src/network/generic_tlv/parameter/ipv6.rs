use std::convert::TryFrom;
use std::net::Ipv6Addr;

use byteorder::{ByteOrder, NetworkEndian};

use crate::network::generic_tlv::{GenericTryFromTLVError, TLV};

/// Type ID for this parameter.
pub const TYPE_ID: u16 = 6;
/// Length of this parameter. Must always be 20.
const STATIC_SIZE: u16 = super::HEADER_SIZE + 16;

/// Implements the `IPv6` parameter. See also [RFC 4960 § 3.3.2.1].
///
/// This parameter contains an IPv6 address of the sending endpoint. It is binary encoded.
///
/// [RFC 4960 § 3.3.2.1]: https://tools.ietf.org/html/rfc4960#section-3.3.2.1
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct IPv6<'bytes> {
    /// The base TLV-parameter this specialized parameter is based upon.
    tlv: TLV<'bytes>,
}

impl IPv6<'_> {
    /// Returns the IP contained in this parameter in form of an [Ipv6Addr]
    ///
    /// [Ipv4Addr]: https://doc.rust-lang.org/std/net/struct.Ipv6Addr.html
    #[allow(clippy::indexing_slicing)]
    pub fn ip(&self) -> Ipv6Addr {
        let bytes = self.tlv.value();
        Ipv6Addr::new(
            NetworkEndian::read_u16(&bytes[0..=1]),
            NetworkEndian::read_u16(&bytes[2..=3]),
            NetworkEndian::read_u16(&bytes[4..=5]),
            NetworkEndian::read_u16(&bytes[6..=7]),
            NetworkEndian::read_u16(&bytes[8..=9]),
            NetworkEndian::read_u16(&bytes[10..=11]),
            NetworkEndian::read_u16(&bytes[12..=13]),
            NetworkEndian::read_u16(&bytes[14..=15]),
        )
    }
}

impl<'bytes> TryFrom<TLV<'bytes>> for IPv6<'bytes> {
    type Error = GenericTryFromTLVError;

    fn try_from(tlv: TLV<'bytes>) -> Result<Self, Self::Error> {
        tlv.verify_static_length(TYPE_ID, STATIC_SIZE)?;

        Ok(Self { tlv })
    }
}

impl<'bytes> From<IPv6<'bytes>> for TLV<'bytes> {
    fn from(ipv6: IPv6<'bytes>) -> Self {
        ipv6.tlv
    }
}

#[cfg(test)]
mod tests {
    use std::convert::TryFrom;

    use super::{IPv6, Ipv6Addr, STATIC_SIZE, TLV, TYPE_ID};

    #[test]
    fn from_tlv() {
        let tlv = TLV::try_from(
            &[
                0, 6, 0, 20, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0xff, 0xff, 0xc0, 0x0a, 0x02, 0xff,
            ][..],
        )
        .unwrap();

        assert_eq!(tlv.ty(), TYPE_ID);
        assert_eq!(tlv.length(), STATIC_SIZE);

        let p = IPv6::try_from(tlv).unwrap();

        assert_eq!(Ipv6Addr::new(0, 0, 0, 0, 0, 0xffff, 0xc00a, 0x02ff), p.ip())
    }
}
