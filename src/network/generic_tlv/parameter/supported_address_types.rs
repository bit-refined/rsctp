use std::convert::TryFrom;

use byteorder::{ByteOrder, NetworkEndian};

use crate::network::generic_tlv::{
    parameter::host_name_address::TYPE_ID as TYPE_ID_HOSTNAME, parameter::ipv4::TYPE_ID as TYPE_ID_IPV4,
    parameter::ipv6::TYPE_ID as TYPE_ID_IPV6, GenericTryFromTLVError, TLV,
};

/// Type ID for this parameter.
pub const TYPE_ID: u16 = 12;
/// Minimum length for this parameter. 4 bytes header, at least 1 address type with 2 bytes each.
const MINIMUM_SIZE: u16 = super::HEADER_SIZE + 2;
/// Maximum length for this parameter. 4 bytes header, at most 3 address types with 2 bytes each.
const MAXIMUM_SIZE: u16 = super::HEADER_SIZE + 2 + 2 + 2;

#[derive(Clone, Debug, PartialEq, Eq)]
/// Implements the `Supported Address Types` parameter. See also [RFC 4960 § 3.3.2.1].
///
/// The sender of INIT uses this parameter to list all the address types it can support.
///
/// [RFC 4960 § 3.3.2.1]: https://tools.ietf.org/html/rfc4960#section-3.3.2.1
pub struct SupportedAddressTypes<'bytes> {
    /// The base TLV-parameter this specialized parameter is based upon.
    tlv: TLV<'bytes>,
    /// Is true if [IPv4] is supported.
    ///
    /// [IPv4]: ../ipv4/struct.IPv4.html
    ipv4: bool,
    /// Is true if [IPv6] is supported.
    ///
    /// [IPV6]: ../ipv6/struct.IPv6.html
    ipv6: bool,
    /// Is true if [HostName] is supported.
    ///
    /// [HostName]: ../host_name_address/struct.HostNameAddress.html
    host_name_address: bool,
}

impl<'bytes> SupportedAddressTypes<'bytes> {
    /// Returns true if IPv4 is supported.
    pub const fn ipv4(&self) -> bool {
        self.ipv4
    }

    /// Returns true if IPv6 is supported.
    pub const fn ipv6(&self) -> bool {
        self.ipv6
    }

    /// Returns true if HostName is supported.
    pub const fn hostname(&self) -> bool {
        self.host_name_address
    }
}

/// Error returned by `TryFrom` for `SupportAddressTypes`.
#[derive(Copy, Clone, Debug)]
pub enum TrySupportedAddressTypesFromTLVError {
    /// Verification error when trying to convert a generic TLV into a specialized one.
    Generic(GenericTryFromTLVError),
    /// Encountered multiple IPv4 address types.
    MultipleIPv4,
    /// Encountered multiple IPv6 address types.
    MultipleIPv6,
    /// Encountered multiple HostName address types.
    MultipleHostNames,
    /// Encountered unknown address type definition.
    UnknownAddressType,
}

impl<'bytes> TryFrom<TLV<'bytes>> for SupportedAddressTypes<'bytes> {
    type Error = TrySupportedAddressTypesFromTLVError;

    fn try_from(tlv: TLV<'bytes>) -> Result<Self, Self::Error> {
        tlv.verify_min_length(TYPE_ID, MINIMUM_SIZE)?;

        if tlv.length() > MAXIMUM_SIZE {
            return Err(TrySupportedAddressTypesFromTLVError::Generic(
                GenericTryFromTLVError::InvalidLength,
            ));
        }

        let (mut ipv4, mut ipv6, mut host_name_address) = (false, false, false);

        let chunks = tlv.value().chunks_exact(2);

        for chunk in chunks {
            match NetworkEndian::read_u16(chunk) {
                TYPE_ID_IPV4 => {
                    if ipv4 {
                        // parameter defined multiple times
                        return Err(TrySupportedAddressTypesFromTLVError::MultipleIPv4);
                    }
                    ipv4 = true
                }
                TYPE_ID_IPV6 => {
                    if ipv6 {
                        // parameter defined multiple times
                        return Err(TrySupportedAddressTypesFromTLVError::MultipleIPv6);
                    }
                    ipv6 = true
                }
                TYPE_ID_HOSTNAME => {
                    if host_name_address {
                        // parameter defined multiple times
                        return Err(TrySupportedAddressTypesFromTLVError::MultipleHostNames);
                    }
                    host_name_address = true
                }
                _ => return Err(TrySupportedAddressTypesFromTLVError::UnknownAddressType),
            }
        }

        Ok(Self {
            tlv,
            ipv4,
            ipv6,
            host_name_address,
        })
    }
}

impl<'bytes> From<SupportedAddressTypes<'bytes>> for TLV<'bytes> {
    fn from(supported_address_types: SupportedAddressTypes<'bytes>) -> Self {
        supported_address_types.tlv
    }
}

impl From<GenericTryFromTLVError> for TrySupportedAddressTypesFromTLVError {
    fn from(e: GenericTryFromTLVError) -> Self {
        TrySupportedAddressTypesFromTLVError::Generic(e)
    }
}

#[cfg(test)]
mod tests {
    use std::convert::TryFrom;

    use super::{SupportedAddressTypes, MAXIMUM_SIZE, MINIMUM_SIZE, TLV, TYPE_ID};

    #[test]
    fn from_tlv_only_ipv4() {
        let tlv = TLV::try_from(&[0, 12, 0, 6, 0, 5][..]).unwrap();

        assert_eq!(tlv.ty(), TYPE_ID);
        assert!(tlv.length() >= MINIMUM_SIZE);
        assert!(tlv.length() <= MAXIMUM_SIZE);
        assert_eq!(tlv.length(), 6);

        let p = SupportedAddressTypes::try_from(tlv).unwrap();

        assert!(p.ipv4());
        assert!(!p.ipv6());
        assert!(!p.hostname());
    }

    #[test]
    fn from_tlv_all_three() {
        let tlv = TLV::try_from(&[0, 12, 0, 10, 0, 5, 0, 6, 0, 11][..]).unwrap();

        assert_eq!(tlv.ty(), TYPE_ID);
        assert!(tlv.length() >= MINIMUM_SIZE);
        assert!(tlv.length() <= MAXIMUM_SIZE);
        assert_eq!(tlv.length(), 10);

        let p = SupportedAddressTypes::try_from(tlv).unwrap();

        assert!(p.ipv4());
        assert!(p.ipv6());
        assert!(p.hostname());
    }

    #[test]
    fn from_tlv_all_three_scrambled() {
        let tlv = TLV::try_from(&[0, 12, 0, 10, 0, 5, 0, 11, 0, 6][..]).unwrap();

        assert_eq!(tlv.ty(), TYPE_ID);
        assert!(tlv.length() >= MINIMUM_SIZE);
        assert!(tlv.length() <= MAXIMUM_SIZE);
        assert_eq!(tlv.length(), 10);

        let p = SupportedAddressTypes::try_from(tlv).unwrap();

        assert!(p.ipv4());
        assert!(p.ipv6());
        assert!(p.hostname());
    }
}
