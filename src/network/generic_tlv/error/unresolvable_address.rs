use crate::network::generic_tlv::TLV;

use super::super::AddrType;

/// Cause code for this error.
pub const CAUSE_CODE: u16 = 5;

/// Implements the `Unresolvable Address` error.
///
/// Indicates that the sender is not able to resolve the specified address parameter (e.g., type of
/// address is not supported by the sender). This is usually sent in combination with or within an
/// `ABORT`.
#[derive(Debug, PartialEq, Eq)]
pub struct UnresolvableAddress<'bytes> {
    /// The base TLV-parameter this specialized parameter is based upon.
    tlv: TLV<'bytes>,
    /// Contains the complete Type, Length, and Value of the address parameter (or Host Name
    /// parameter) that contains the unresolvable address or host name.
    addr: AddrType<'bytes>,
}
