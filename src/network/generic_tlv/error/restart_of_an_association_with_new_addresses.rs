use std::convert::TryFrom;

use crate::network::generic_tlv::parameter::host_name_address::TryHostNameAddressFromTLVError;
use crate::network::generic_tlv::{
    parameter::{
        host_name_address::{HostNameAddress, TYPE_ID as TYPE_ID_HOST_NAME_ADDRESS},
        ipv4::{IPv4, TYPE_ID as TYPE_ID_IPV4},
        ipv6::{IPv6, TYPE_ID as TYPE_ID_IPV6},
        is_recognized,
    },
    AddrType, GenericTryFromTLVError, ParsedTLV, TryTLVFromBytesError, TLV,
};

/// Cause code for this error.
pub const CAUSE_CODE: u16 = 11;
/// Minimum size for this error. 4 bytes header, 4 bytes for header of 1 new address and 1 byte for
/// the smallest one of them, the hostname, which can be as short as a single byte (`\n`).
const MINIMUM_SIZE: u16 = super::HEADER_SIZE + super::super::parameter::HEADER_SIZE + 1;

/// Implements the `Restart of an Association with New Addresses` error.
///
/// An `INIT` was received on an existing association. But the `INIT` added addresses to the
/// association that were previously NOT part of the association. The new addresses are listed in
/// the error code. This error is normally sent as part of an `ABORT` refusing the `INIT`.
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct RestartOfAnAssociationWithNewAddresses<'bytes> {
    /// The base TLV-parameter this specialized parameter is based upon.
    tlv: TLV<'bytes>,
}

/// Error returned by `.new_addresses()` for `RestartOfAnAssociationWithNewAddresses`.
#[derive(Copy, Clone)]
#[allow(variant_size_differences)]
pub enum NewAddressesError {
    /// Verification error when trying to convert a generic TLV into a specialized one.
    ParameterParsing(GenericTryFromTLVError),
    /// Error when trying to convert bytes into a generic TLV.
    TLVParsing(TryTLVFromBytesError),
    /// Error when trying to parse a given `HostNameAddress` parameter.
    HostNameParsing(TryHostNameAddressFromTLVError),
    /// Encountered invalid parameter.
    UnrecognizedParameterType,
}

impl<'bytes> RestartOfAnAssociationWithNewAddresses<'bytes> {
    /// Each New Address TLV is an exact copy of the TLV that was found in the `INIT` chunk that
    /// was new, including the Parameter Type and the Parameter Length.
    pub fn new_addresses(&self) -> Result<Vec<AddrType<'bytes>>, NewAddressesError> {
        let mut addrs = Vec::new();

        match TLV::from_list(self.tlv.value(), is_recognized)? {
            ParsedTLV::AllRecognized(params) => {
                for param in params {
                    match param.ty() {
                        TYPE_ID_IPV4 => {
                            let specialized = IPv4::try_from(param)?;
                            addrs.push(AddrType::IPv4(specialized))
                        }
                        TYPE_ID_IPV6 => {
                            let specialized = IPv6::try_from(param)?;
                            addrs.push(AddrType::IPv6(specialized))
                        }
                        TYPE_ID_HOST_NAME_ADDRESS => {
                            let specialized = HostNameAddress::try_from(param)?;
                            addrs.push(AddrType::HostNameAddress(specialized))
                        }
                        _ => return Err(NewAddressesError::UnrecognizedParameterType),
                    }
                }

                Ok(addrs)
            }
            ParsedTLV::SomeRecognized(_)
            | ParsedTLV::SomeRecognizedAndReport(_, _)
            | ParsedTLV::Stop(_)
            | ParsedTLV::StopAndReport(_, _) => Err(NewAddressesError::UnrecognizedParameterType),
        }
    }
}

impl<'bytes> TryFrom<TLV<'bytes>> for RestartOfAnAssociationWithNewAddresses<'bytes> {
    type Error = GenericTryFromTLVError;

    fn try_from(tlv: TLV<'bytes>) -> Result<Self, Self::Error> {
        tlv.verify_min_length(CAUSE_CODE, MINIMUM_SIZE)?;

        Ok(Self { tlv })
    }
}

impl<'bytes> From<RestartOfAnAssociationWithNewAddresses<'bytes>> for TLV<'bytes> {
    fn from(restart_of_an_association_with_new_addresses: RestartOfAnAssociationWithNewAddresses<'bytes>) -> Self {
        restart_of_an_association_with_new_addresses.tlv
    }
}

impl From<GenericTryFromTLVError> for NewAddressesError {
    fn from(e: GenericTryFromTLVError) -> Self {
        NewAddressesError::ParameterParsing(e)
    }
}

impl From<TryTLVFromBytesError> for NewAddressesError {
    fn from(e: TryTLVFromBytesError) -> Self {
        NewAddressesError::TLVParsing(e)
    }
}

impl From<TryHostNameAddressFromTLVError> for NewAddressesError {
    fn from(e: TryHostNameAddressFromTLVError) -> Self {
        NewAddressesError::HostNameParsing(e)
    }
}
