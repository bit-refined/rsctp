use std::convert::TryFrom;

use byteorder::{ByteOrder, NetworkEndian};

use crate::network::generic_tlv::{GenericTryFromTLVError, TLV};

/// Cause code for this error.
pub const CAUSE_CODE: u16 = 3;
/// Static size for this error. 4 bytes header and 4 bytes for the measure of staleness.
const STATIC_SIZE: u16 = super::HEADER_SIZE + 4;

/// Implements the `Stale Cookie` error.
///
/// Indicates the receipt of a valid State Cookie that has expired.
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct StaleCookieError<'bytes> {
    /// The base TLV-parameter this specialized parameter is based upon.
    tlv: TLV<'bytes>,
}

impl<'bytes> StaleCookieError<'bytes> {
    /// This field contains the difference, in microseconds, between the current time and the time
    /// the State Cookie expired.
    ///
    /// The sender of this error cause MAY choose to report how long past expiration the State
    /// Cookie is by including a non-zero value in the Measure of Staleness field. If the sender
    /// does not wish to provide this information, it should set the Measure of Staleness field to
    /// the value of zero.
    pub fn measure_of_staleness(&self) -> u32 {
        #[allow(clippy::indexing_slicing)]
        NetworkEndian::read_u32(&self.tlv.value()[0..=3])
    }
}

impl<'bytes> TryFrom<TLV<'bytes>> for StaleCookieError<'bytes> {
    type Error = GenericTryFromTLVError;

    fn try_from(tlv: TLV<'bytes>) -> Result<Self, Self::Error> {
        tlv.verify_static_length(CAUSE_CODE, STATIC_SIZE)?;

        Ok(Self { tlv })
    }
}

impl<'bytes> From<StaleCookieError<'bytes>> for TLV<'bytes> {
    fn from(stale_cookie_error: StaleCookieError<'bytes>) -> Self {
        stale_cookie_error.tlv
    }
}

#[cfg(test)]
mod tests {
    use std::convert::TryFrom;

    use super::{StaleCookieError, CAUSE_CODE, STATIC_SIZE, TLV};

    #[test]
    fn from_tlv() {
        let tlv = TLV::try_from(&[0, 3, 0, 8, 0, 0, 5, 57][..]).unwrap();

        assert_eq!(tlv.ty(), CAUSE_CODE);
        assert_eq!(tlv.length(), STATIC_SIZE);

        let e = StaleCookieError::try_from(tlv).unwrap();

        assert_eq!(e.measure_of_staleness(), 1337);
    }
}
