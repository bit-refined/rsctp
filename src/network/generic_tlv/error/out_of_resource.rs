use std::convert::TryFrom;

use crate::network::generic_tlv::{GenericTryFromTLVError, TLV};

/// Cause code for this error.
pub const CAUSE_CODE: u16 = 4;
/// Static size for this error. 4 bytes header.
const STATIC_SIZE: u16 = super::HEADER_SIZE;

/// Implements the `Out of Resource` error.
///
/// Indicates that the sender is out of resource. This is usually sent in combination with or
/// within an `ABORT`.
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct OutOfResource<'bytes> {
    /// The base TLV-parameter this specialized parameter is based upon.
    tlv: TLV<'bytes>,
}

impl<'bytes> TryFrom<TLV<'bytes>> for OutOfResource<'bytes> {
    type Error = GenericTryFromTLVError;

    fn try_from(tlv: TLV<'bytes>) -> Result<Self, Self::Error> {
        tlv.verify_static_length(CAUSE_CODE, STATIC_SIZE)?;

        Ok(Self { tlv })
    }
}

impl<'bytes> From<OutOfResource<'bytes>> for TLV<'bytes> {
    fn from(out_of_resource: OutOfResource<'bytes>) -> Self {
        out_of_resource.tlv
    }
}

#[cfg(test)]
mod tests {
    use std::convert::TryFrom;

    use super::{OutOfResource, CAUSE_CODE, STATIC_SIZE, TLV};

    #[test]
    fn from_tlv() {
        let tlv = TLV::try_from(&[0, 4, 0, 4][..]).unwrap();

        assert_eq!(tlv.ty(), CAUSE_CODE);
        assert_eq!(tlv.length(), STATIC_SIZE);

        let _ = OutOfResource::try_from(tlv).unwrap();
    }
}
