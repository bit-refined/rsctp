use std::convert::TryFrom;

use crate::network::generic_tlv::{GenericTryFromTLVError, TLV};

/// Cause code for this error.
pub const CAUSE_CODE: u16 = 13;
/// Minimum size for this error.
const MINIMUM_SIZE: u16 = super::HEADER_SIZE;

/// Implements the `Protocol Violation` error.
///
/// This error cause MAY be included in `ABORT` chunks that are sent because an SCTP endpoint
/// detects a protocol violation of the peer that is not covered by another error cause. An
/// implementation MAY provide additional information specifying what kind of protocol violation
/// has been detected.
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct ProtocolViolation<'bytes> {
    /// The base TLV-parameter this specialized parameter is based upon.
    tlv: TLV<'bytes>,
}

impl<'bytes> ProtocolViolation<'bytes> {
    /// Additional information specifying what kind of protocol violation has been detected.
    pub fn additional_information(&self) -> &[u8] {
        self.tlv.value()
    }
}

impl<'bytes> TryFrom<TLV<'bytes>> for ProtocolViolation<'bytes> {
    type Error = GenericTryFromTLVError;

    fn try_from(tlv: TLV<'bytes>) -> Result<Self, Self::Error> {
        tlv.verify_min_length(CAUSE_CODE, MINIMUM_SIZE)?;

        Ok(Self { tlv })
    }
}

impl<'bytes> From<ProtocolViolation<'bytes>> for TLV<'bytes> {
    fn from(protocol_violation: ProtocolViolation<'bytes>) -> Self {
        protocol_violation.tlv
    }
}

#[cfg(test)]
mod tests {
    use std::convert::TryFrom;

    use super::{ProtocolViolation, CAUSE_CODE, MINIMUM_SIZE, TLV};

    #[test]
    fn from_tlv() {
        let tlv = TLV::try_from(&[0, 13, 0, 6, 42, 24][..]).unwrap();

        assert_eq!(tlv.ty(), CAUSE_CODE);
        assert!(tlv.length() >= MINIMUM_SIZE);
        assert_eq!(tlv.length(), 6);

        let e = ProtocolViolation::try_from(tlv).unwrap();

        assert_eq!(e.additional_information().to_vec(), vec![42, 24]);
    }
}
