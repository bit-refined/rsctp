use std::convert::TryFrom;

use byteorder::{ByteOrder, NetworkEndian};

use crate::network::generic_tlv::{GenericTryFromTLVError, TLV};

/// Cause code for this error.
pub const CAUSE_CODE: u16 = 9;
/// Static size for this error. 4 bytes header, 4 bytes sequence number.
const STATIC_SIZE: u16 = super::HEADER_SIZE + 4;

/// Implements the `No User Data` error.
///
/// This error cause is returned to the originator of a `DATA` chunk if a received `DATA` chunk has
/// no user data. This cause code is normally returned in an `ABORT` chunk.
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct NoUserData<'bytes> {
    /// The base TLV-parameter this specialized parameter is based upon.
    tlv: TLV<'bytes>,
}

impl<'bytes> NoUserData<'bytes> {
    /// Contains the TSN of the `DATA` chunk received with no user data field.
    pub fn transmission_sequence_number(&self) -> u32 {
        #[allow(clippy::indexing_slicing)]
        NetworkEndian::read_u32(&self.tlv.value()[0..=3])
    }
}

impl<'bytes> TryFrom<TLV<'bytes>> for NoUserData<'bytes> {
    type Error = GenericTryFromTLVError;

    fn try_from(tlv: TLV<'bytes>) -> Result<Self, Self::Error> {
        tlv.verify_static_length(CAUSE_CODE, STATIC_SIZE)?;

        Ok(Self { tlv })
    }
}

impl<'bytes> From<NoUserData<'bytes>> for TLV<'bytes> {
    fn from(no_user_data: NoUserData<'bytes>) -> Self {
        no_user_data.tlv
    }
}

#[cfg(test)]
mod tests {
    use std::convert::TryFrom;

    use super::{NoUserData, CAUSE_CODE, STATIC_SIZE, TLV};

    #[test]
    fn from_tlv() {
        let tlv = TLV::try_from(&[0, 9, 0, 8, 0, 0, 5, 57][..]).unwrap();

        assert_eq!(tlv.ty(), CAUSE_CODE);
        assert_eq!(tlv.length(), STATIC_SIZE);

        let e = NoUserData::try_from(tlv).unwrap();

        assert_eq!(e.transmission_sequence_number(), 1337);
    }
}
