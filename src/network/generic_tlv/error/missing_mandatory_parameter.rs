use std::convert::TryFrom;

use byteorder::{ByteOrder, NetworkEndian};

use crate::network::generic_tlv::{GenericTryFromTLVError, TLV};

/// Cause code for this error.
pub const CAUSE_CODE: u16 = 2;
/// Minimum size for this error.
/// 4 bytes header, 4 bytes for the number of types and 2 for at least one.
const MINIMUM_SIZE: u16 = super::HEADER_SIZE + 4 + 2;
/// The `number_of_missing_params` value may never be higher than this value to prevent overflowing.
const MAX_TYPES: u32 = 0x7FF7; // u16::max_value() as u32 / 2 - 8;

/// Implements the `Missing Mandatory Parameter` error.
///
/// Indicates that one or more mandatory TLV parameters are missing in a received `INIT` or
/// `INIT_ACK`.
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct MissingMandatoryParameter<'bytes> {
    /// The base TLV-parameter this specialized parameter is based upon.
    tlv: TLV<'bytes>,
}

impl<'bytes> MissingMandatoryParameter<'bytes> {
    /// This field contains the number of parameters contained
    /// in the Cause-Specific Information field.
    pub fn number_of_missing_params(&self) -> u32 {
        #[allow(clippy::indexing_slicing)]
        NetworkEndian::read_u32(&self.tlv.value()[0..=3])
    }

    /// Each field will contain the missing mandatory parameter number.
    pub fn missing_param_types(&self) -> Vec<u16> {
        let num = self.number_of_missing_params() as usize;
        let mut params = Vec::with_capacity(num);

        #[allow(clippy::indexing_slicing)]
        let value = &self.tlv.value()[4..];

        value
            .chunks_exact(2)
            .for_each(|slice| params.push(NetworkEndian::read_u16(slice)));

        params
    }
}

/// Error returned by `TryFrom` for `MissingMandatoryParameter`.
#[derive(Clone, Copy, Debug)]
pub enum TryMissingMandatoryParameterFromTLVError {
    /// Verification error when trying to convert a generic TLV into a specialized one.
    Generic(GenericTryFromTLVError),
    /// Missing parameter value too high.
    TooManyParameters,
}

impl<'bytes> TryFrom<TLV<'bytes>> for MissingMandatoryParameter<'bytes> {
    type Error = TryMissingMandatoryParameterFromTLVError;

    #[allow(clippy::integer_arithmetic)]
    fn try_from(tlv: TLV<'bytes>) -> Result<Self, Self::Error> {
        tlv.verify_min_length(CAUSE_CODE, MINIMUM_SIZE)?;

        let error = Self { tlv };

        let missing_params = error.number_of_missing_params();

        if missing_params > MAX_TYPES {
            return Err(TryMissingMandatoryParameterFromTLVError::TooManyParameters);
        }

        if missing_params * 2 + 8 != u32::from(error.tlv.length()) {
            return Err(TryMissingMandatoryParameterFromTLVError::Generic(
                GenericTryFromTLVError::InvalidLength,
            ));
        }

        Ok(error)
    }
}

impl<'bytes> From<MissingMandatoryParameter<'bytes>> for TLV<'bytes> {
    fn from(missing_mandatory_parameter: MissingMandatoryParameter<'bytes>) -> Self {
        missing_mandatory_parameter.tlv
    }
}

impl From<GenericTryFromTLVError> for TryMissingMandatoryParameterFromTLVError {
    fn from(e: GenericTryFromTLVError) -> Self {
        TryMissingMandatoryParameterFromTLVError::Generic(e)
    }
}

#[cfg(test)]
mod tests {
    use std::convert::TryFrom;

    use super::{MissingMandatoryParameter, CAUSE_CODE, MINIMUM_SIZE, TLV};

    #[test]
    fn from_tlv() {
        let tlv = TLV::try_from(&[0, 2, 0, 12, 0, 0, 0, 2, 0, 42, 0, 24][..]).unwrap();

        assert_eq!(tlv.ty(), CAUSE_CODE);
        assert!(tlv.length() >= MINIMUM_SIZE);
        assert_eq!(tlv.length(), 12);

        let e = MissingMandatoryParameter::try_from(tlv).unwrap();

        assert_eq!(e.number_of_missing_params(), 2);
        assert_eq!(e.missing_param_types(), vec![42, 24]);
    }
}
