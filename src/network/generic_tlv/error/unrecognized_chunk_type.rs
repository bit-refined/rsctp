use std::convert::TryFrom;

use crate::network::chunk::{BytesToChunkError, Chunk, HEADER_SIZE as CHUNK_HEADER_SIZE};
use crate::network::generic_tlv::{GenericTryFromTLVError, TLV};

/// Cause code for this error.
pub const CAUSE_CODE: u16 = 6;
/// Minimum size for this error. 4 bytes error header, 4 bytes chunk header.
const MINIMUM_SIZE: u16 = super::HEADER_SIZE + CHUNK_HEADER_SIZE;

/// Implements the `Unrecognized Chunk Type` error.
///
/// This error cause is returned to the originator of the chunk if the receiver does not understand
/// the chunk and the upper bits of the 'Chunk Type' are set to 01 or 11.
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct UnrecognizedChunkType<'bytes> {
    /// The base TLV-parameter this specialized parameter is based upon.
    tlv: TLV<'bytes>,
}

impl<'bytes> TryFrom<TLV<'bytes>> for UnrecognizedChunkType<'bytes> {
    type Error = GenericTryFromTLVError;

    fn try_from(tlv: TLV<'bytes>) -> Result<Self, Self::Error> {
        tlv.verify_min_length(CAUSE_CODE, MINIMUM_SIZE)?;

        Ok(Self { tlv })
    }
}

impl<'bytes> From<UnrecognizedChunkType<'bytes>> for TLV<'bytes> {
    fn from(unrecognized_chunk_type: UnrecognizedChunkType<'bytes>) -> Self {
        unrecognized_chunk_type.tlv
    }
}

impl<'bytes> TryFrom<UnrecognizedChunkType<'bytes>> for Chunk<'bytes> {
    type Error = BytesToChunkError;

    fn try_from(unrecognized_chunk_type: UnrecognizedChunkType<'bytes>) -> Result<Self, Self::Error> {
        Chunk::try_from(unrecognized_chunk_type.tlv.value())
    }
}

#[cfg(test)]
mod tests {
    use std::convert::TryFrom;

    use super::{Chunk, UnrecognizedChunkType, CAUSE_CODE, MINIMUM_SIZE, TLV};

    #[test]
    fn from_tlv() {
        let tlv = TLV::try_from(&[0, 6, 0, 10, 120, 247, 0, 6, 42, 24][..]).unwrap();

        assert_eq!(tlv.ty(), CAUSE_CODE);
        assert!(tlv.length() >= MINIMUM_SIZE);
        assert_eq!(tlv.length(), 10);

        let e = UnrecognizedChunkType::try_from(tlv).unwrap();
        let c = Chunk::try_from(e).unwrap();

        assert_eq!(c.ty(), 120);
        assert_eq!(c.flags(), 247);
        assert_eq!(c.length(), 6);
        assert_eq!(c.value().to_vec(), vec![42, 24])
    }
}
