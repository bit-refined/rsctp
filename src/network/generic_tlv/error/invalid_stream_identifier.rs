use std::convert::TryFrom;

use byteorder::{ByteOrder, NetworkEndian};

use crate::network::generic_tlv::{GenericTryFromTLVError, TLV};

/// Cause code for this error.
pub const CAUSE_CODE: u16 = 1;
/// Static size for this error. 4 bytes header, 2 bytes identifier and 2 reserved bytes.
const STATIC_SIZE: u16 = super::HEADER_SIZE + 2 + 2;

/// Implements the `Invalid Stream Identifier` error.
///
/// Indicates endpoint received a `DATA` chunk sent to a nonexistent stream.
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct InvalidStreamIdentifier<'bytes> {
    /// The base TLV-parameter this specialized parameter is based upon.
    tlv: TLV<'bytes>,
}

impl InvalidStreamIdentifier<'_> {
    /// Returns the Stream Identifier of the `DATA` chunk which caused this error.
    pub fn identifier(&self) -> u16 {
        #[allow(clippy::indexing_slicing)]
        NetworkEndian::read_u16(&self.tlv.value()[0..=1])
    }
}

impl<'bytes> TryFrom<TLV<'bytes>> for InvalidStreamIdentifier<'bytes> {
    type Error = GenericTryFromTLVError;

    fn try_from(tlv: TLV<'bytes>) -> Result<Self, Self::Error> {
        tlv.verify_static_length(CAUSE_CODE, STATIC_SIZE)?;

        Ok(Self { tlv })
    }
}

impl<'bytes> From<InvalidStreamIdentifier<'bytes>> for TLV<'bytes> {
    fn from(invalid_stream_identifier: InvalidStreamIdentifier<'bytes>) -> Self {
        invalid_stream_identifier.tlv
    }
}

#[cfg(test)]
mod tests {
    use std::convert::TryFrom;

    use super::{InvalidStreamIdentifier, CAUSE_CODE, STATIC_SIZE, TLV};

    #[test]
    fn from_tlv() {
        let tlv = TLV::try_from(&[0, 1, 0, 8, 5, 57, 0, 0][..]).unwrap();

        assert_eq!(tlv.ty(), CAUSE_CODE);
        assert_eq!(tlv.length(), STATIC_SIZE);

        let e = InvalidStreamIdentifier::try_from(tlv).unwrap();

        assert_eq!(e.identifier(), 1337);
    }
}
