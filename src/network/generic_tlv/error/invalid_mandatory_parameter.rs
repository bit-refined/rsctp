use std::convert::TryFrom;

use crate::network::generic_tlv::{GenericTryFromTLVError, TLV};

/// Cause code for this error.
pub const CAUSE_CODE: u16 = 7;
/// Static size for this error. 4 bytes header with no additional data.
const STATIC_SIZE: u16 = super::HEADER_SIZE;

/// Implements the `Invalid Mandatory Parameter` error.
///
/// This error cause is returned to the originator of an INIT or INIT ACK chunk
/// when one of the mandatory parameters is set to an invalid value.
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct InvalidMandatoryParameter<'bytes> {
    /// The base TLV-parameter this specialized parameter is based upon.
    tlv: TLV<'bytes>,
}

impl<'bytes> TryFrom<TLV<'bytes>> for InvalidMandatoryParameter<'bytes> {
    type Error = GenericTryFromTLVError;

    fn try_from(tlv: TLV<'bytes>) -> Result<Self, Self::Error> {
        tlv.verify_static_length(CAUSE_CODE, STATIC_SIZE)?;

        Ok(Self { tlv })
    }
}

impl<'bytes> From<InvalidMandatoryParameter<'bytes>> for TLV<'bytes> {
    fn from(invalid_mandatory_parameter: InvalidMandatoryParameter<'bytes>) -> Self {
        invalid_mandatory_parameter.tlv
    }
}

#[cfg(test)]
mod tests {
    use std::convert::TryFrom;

    use super::{InvalidMandatoryParameter, CAUSE_CODE, STATIC_SIZE, TLV};

    #[test]
    fn from_tlv() {
        let tlv = TLV::try_from(&[0, 7, 0, 4][..]).unwrap();

        assert_eq!(tlv.ty(), CAUSE_CODE);
        assert_eq!(tlv.length(), STATIC_SIZE);

        let _ = InvalidMandatoryParameter::try_from(tlv).unwrap();
    }
}
