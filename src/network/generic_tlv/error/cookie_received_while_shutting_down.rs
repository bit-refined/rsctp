use std::convert::TryFrom;

use crate::network::generic_tlv::{GenericTryFromTLVError, TLV};

/// Cause code for this error.
pub const CAUSE_CODE: u16 = 10;
/// Static size for this error. 4 bytes header with no additional data.
const STATIC_SIZE: u16 = super::HEADER_SIZE;

/// Implements the `Cookie Received While Shutting Down` error.
///
/// A `COOKIE_ECHO` was received while the endpoint was in the `SHUTDOWN-ACK-SENT` state. This
/// error is usually returned in an `ERROR` chunk bundled with the retransmitted `SHUTDOWN_ACK`.
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct CookieReceivedWhileShuttingDown<'bytes> {
    /// The base TLV-parameter this specialized parameter is based upon.
    tlv: TLV<'bytes>,
}

impl<'bytes> TryFrom<TLV<'bytes>> for CookieReceivedWhileShuttingDown<'bytes> {
    type Error = GenericTryFromTLVError;

    fn try_from(tlv: TLV<'bytes>) -> Result<Self, Self::Error> {
        tlv.verify_static_length(CAUSE_CODE, STATIC_SIZE)?;

        Ok(Self { tlv })
    }
}

impl<'bytes> From<CookieReceivedWhileShuttingDown<'bytes>> for TLV<'bytes> {
    fn from(cookie_received_while_shutting_down: CookieReceivedWhileShuttingDown<'bytes>) -> Self {
        cookie_received_while_shutting_down.tlv
    }
}

#[cfg(test)]
mod tests {
    use std::convert::TryFrom;

    use super::{CookieReceivedWhileShuttingDown, CAUSE_CODE, STATIC_SIZE, TLV};

    #[test]
    fn from_tlv() {
        let tlv = TLV::try_from(&[0, 10, 0, 4][..]).unwrap();

        assert_eq!(tlv.ty(), CAUSE_CODE);
        assert_eq!(tlv.length(), STATIC_SIZE);

        let _ = CookieReceivedWhileShuttingDown::try_from(tlv).unwrap();
    }
}
