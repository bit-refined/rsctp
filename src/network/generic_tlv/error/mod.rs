//pub(crate) use self::{
//    cookie_received_while_shutting_down::CookieReceivedWhileShuttingDown,
//    invalid_mandatory_parameter::InvalidMandatoryParameter, invalid_stream_identifier::InvalidStreamIdentifier,
//    missing_mandatory_parameter::MissingMandatoryParameter, no_user_data::NoUserData, out_of_resource::OutOfResource,
//    protocol_violation::ProtocolViolation,
//    restart_of_an_association_with_new_addresses::RestartOfAnAssociationWithNewAddresses,
//    stale_cookie_error::StaleCookieError, /*unrecognized_chunk_type::UnrecognizedChunkType,*/
//    unrecognized_parameters::UnrecognizedParameters, unresolvable_address::UnresolvableAddress,
//    user_initiated_abort::UserInitiatedAbort,
//};

/// Cookie Received While Shutting Down error.
pub mod cookie_received_while_shutting_down;
/// Invalid Mandatory Parameter error.
pub mod invalid_mandatory_parameter;
/// Invalid Stream Identifier error.
pub mod invalid_stream_identifier;
/// Missing Mandatory Parameter error.
pub mod missing_mandatory_parameter;
/// No User Data error.
pub mod no_user_data;
/// Out of Resource error.
pub mod out_of_resource;
/// Protocol Violation error.
pub mod protocol_violation;
/// Restart of an Association with New Addresses error.
pub mod restart_of_an_association_with_new_addresses;
/// Stale Cookie error.
#[allow(clippy::module_name_repetitions)]
pub mod stale_cookie_error;
/// Unrecognized Chunk Type error.
pub mod unrecognized_chunk_type;
/// Unrecognized Parameters error.
pub mod unrecognized_parameters;
/// Unresolvable Address error.
pub mod unresolvable_address;
/// User Initiated Abort error.
pub mod user_initiated_abort;

/// Error when parsing `Error` from bytes.
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
#[allow(clippy::module_name_repetitions)]
pub enum BytesToErrorError {
    /// Error is too short to be a valid error.
    TooShort,
    /// Error has a invalid length, indicating a [ProtocolViolation].
    ///
    /// [ProtocolViolation]: protocol_violation/struct.ProtocolViolation.html
    InvalidLength,
    /// Error has invalid data, indicating a [ProtocolViolation].
    ///
    /// [ProtocolViolation]: protocol_violation/struct.ProtocolViolation.html
    InvalidData,
}

/// Error header size
pub const HEADER_SIZE: u16 = 4;

/// Returns true if the given `cause_code` has a value
/// with a corresponding RSCTP error `CAUSE_CODE`.
pub fn is_recognized(cause_code: u16) -> bool {
    match cause_code {
        cookie_received_while_shutting_down::CAUSE_CODE
        | invalid_mandatory_parameter::CAUSE_CODE
        | invalid_stream_identifier::CAUSE_CODE
        | missing_mandatory_parameter::CAUSE_CODE
        | no_user_data::CAUSE_CODE
        | out_of_resource::CAUSE_CODE
        | protocol_violation::CAUSE_CODE
        | restart_of_an_association_with_new_addresses::CAUSE_CODE
        | stale_cookie_error::CAUSE_CODE
        | unrecognized_chunk_type::CAUSE_CODE
        | unrecognized_parameters::CAUSE_CODE
        | unresolvable_address::CAUSE_CODE
        | user_initiated_abort::CAUSE_CODE => true,
        _ => false,
    }
}
