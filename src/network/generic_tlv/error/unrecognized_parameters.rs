use crate::network::generic_tlv::TLV;

/// Cause code for this error.
pub const CAUSE_CODE: u16 = 8;
///// Minimum size for this error. 4 bytes error header, 4 bytes chunk header.
//const MINIMUM_SIZE: u16 = ERROR_HEADER_SIZE + PARAMETER_HEADER_SIZE;

/// Implements the `Unrecognized Parameters` error.
///
/// This error cause is returned to the originator of the `INIT_ACK` chunk if the receiver does not
/// recognize one or more Optional TLV parameters in the `INIT_ACK` chunk. This error cause is
/// normally contained in an `ERROR` chunk bundled with the `COOKIE_ECHO` chunk when responding to
/// the `INIT_ACK`, when the sender of the `COOKIE_ECHO` chunk wishes to report unrecognized
/// parameters.
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct UnrecognizedParameters<'bytes> {
    /// The base TLV-parameter this specialized parameter is based upon.
    tlv: TLV<'bytes>,
    /// Contains the unrecognized parameters copied from the `INIT_ACK` chunk complete with TLV.
    unrecognized_parameters: Vec<TLV<'bytes>>,
}
