use std::convert::TryFrom;

use crate::network::generic_tlv::{GenericTryFromTLVError, TLV};

/// Cause code for this error.
pub const CAUSE_CODE: u16 = 12;
/// Minimum size for this error.
const MINIMUM_SIZE: u16 = super::HEADER_SIZE;

/// Implements the `User Initiated Abort` error.
///
/// This error cause MAY be included in `ABORT` chunks that are sent because of an upper-layer
/// request. The upper layer can specify an Upper Layer Abort Reason that is transported by SCTP
/// transparently and MAY be delivered to the upper-layer protocol at the peer.
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct UserInitiatedAbort<'bytes> {
    /// The base TLV-parameter this specialized parameter is based upon.
    tlv: TLV<'bytes>,
}

impl<'bytes> UserInitiatedAbort<'bytes> {
    /// Upper Layer Abort Reason that is transported by SCTP transparently.
    pub fn upper_layer_abort_reason(&self) -> &[u8] {
        self.tlv.value()
    }
}

impl<'bytes> TryFrom<TLV<'bytes>> for UserInitiatedAbort<'bytes> {
    type Error = GenericTryFromTLVError;

    fn try_from(tlv: TLV<'bytes>) -> Result<Self, Self::Error> {
        tlv.verify_min_length(CAUSE_CODE, MINIMUM_SIZE)?;

        Ok(Self { tlv })
    }
}

impl<'bytes> From<UserInitiatedAbort<'bytes>> for TLV<'bytes> {
    fn from(user_initiated_abort: UserInitiatedAbort<'bytes>) -> Self {
        user_initiated_abort.tlv
    }
}

#[cfg(test)]
mod tests {
    use std::convert::TryFrom;

    use super::{UserInitiatedAbort, CAUSE_CODE, MINIMUM_SIZE, TLV};

    #[test]
    fn from_tlv() {
        let tlv = TLV::try_from(&[0, 12, 0, 6, 42, 24][..]).unwrap();

        assert_eq!(tlv.ty(), CAUSE_CODE);
        assert!(tlv.length() >= MINIMUM_SIZE);
        assert_eq!(tlv.length(), 6);

        let e = UserInitiatedAbort::try_from(tlv).unwrap();

        assert_eq!(e.upper_layer_abort_reason().to_vec(), vec![42, 24]);
    }
}
