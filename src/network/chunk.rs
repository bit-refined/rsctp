use std::convert::TryFrom;

use byteorder::{ByteOrder, NetworkEndian};

use crate::network::{
    generic_tlv::{ParsedTLV, TryTLVFromBytesError, TLV},
    padding::total_padded_length,
};

pub use self::{
    abort::Abort, cookie_ack::CookieACK, cookie_echo::CookieEcho, data::Data, error::Error, heartbeat::Heartbeat,
    heartbeat_ack::HeartbeatACK, init::Init, init_ack::InitACK, sack::SACK, shutdown::Shutdown,
    shutdown_ack::ShutdownACK, shutdown_complete::ShutdownComplete,
};

/// `ABORT` chunk.
pub mod abort;
/// `COOKIE_ACK` chunk.
pub mod cookie_ack;
/// `COOKIE_ECHO` chunk.
pub mod cookie_echo;
/// `DATA` chunk.
pub mod data;
/// `ERROR` chunk.
pub mod error;
/// `HEARTBEAT` chunk.
pub mod heartbeat;
/// `HEARTBEAT_ACK` chunk.
pub mod heartbeat_ack;
/// `INIT` chunk.
pub mod init;
/// `INIT_ACK` chunk.
pub mod init_ack;
/// `SACK` chunk.
pub mod sack;
/// `SHUTDOWN` chunk.
pub mod shutdown;
/// `SHUTDOWN_ACK` chunk.
pub mod shutdown_ack;
/// `SHUTDOWN_COMPLETE` chunk.
pub mod shutdown_complete;

/// Chunk header size
pub const HEADER_SIZE: u16 = 4;

/// The figure below illustrates the field format for the chunks to be
/// transmitted in the SCTP packet.  Each chunk is formatted with a Chunk
/// Type field, a chunk-specific Flag field, a Chunk Length field, and a
/// Value field.
///```sh
///      0                   1                   2                   3
///      0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
///     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
///     |   Chunk Type  | Chunk  Flags  |        Chunk Length           |
///     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
///     \                                                               \
///     /                          Chunk Value                          /
///     \                                                               \
///     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
/// ```
#[derive(Clone, Debug, Ord, PartialOrd, Eq, PartialEq)]
pub struct Chunk<'bytes> {
    /// This field contains the exact slice of bytes given by the packet in
    /// which it is located.
    pub bytes: &'bytes [u8],
}

impl<'bytes> Chunk<'bytes> {
    /// This field identifies the type of information contained in the
    /// Chunk Value field. It takes a value from 0 to 254.  The value of
    /// 255 is reserved for future use as an extension field.
    pub const fn ty(&self) -> u8 {
        #[allow(clippy::indexing_slicing)]
        self.bytes[0]
    }

    /// The usage of these bits depends on the Chunk type as given by the
    /// Chunk Type field. Unless otherwise specified, they are set to 0
    /// on transmit and are ignored on receipt.
    pub const fn flags(&self) -> u8 {
        #[allow(clippy::indexing_slicing)]
        self.bytes[1]
    }

    /// Get the value of a single flag.
    pub const fn flag(&self, flag: u8) -> bool {
        self.flags() & flag != 0
    }

    /// This value represents the size of the chunk in bytes, including
    /// the Chunk Type, Chunk Flags, Chunk Length, and Chunk Value fields.
    /// Therefore, if the Chunk Value field is zero-length, the Length field will be set to 4.
    ///
    /// The Chunk Length field does not count any chunk padding.
    pub fn length(&self) -> u16 {
        #[allow(clippy::indexing_slicing)]
        NetworkEndian::read_u16(&self.bytes[2..=3])
    }

    /// This value represents the size of the chunk in bytes, including
    /// the Chunk Type, Chunk Flags, Chunk Length, and Chunk Value fields
    /// with the addition of required padding.
    pub fn padded_length(&self) -> usize {
        total_padded_length(self.length())
    }

    /// The Chunk Value field contains the actual information to be
    /// transferred in the chunk. The usage and format of this field is
    /// dependent on the Chunk Type.
    pub fn value(&self) -> &'bytes [u8] {
        #[allow(clippy::indexing_slicing)]
        &self.bytes[HEADER_SIZE as usize..]
    }

    /// Helper function to verify that this chunk has the specified type and
    /// minimum length. Intended for usage in `TryFrom<Chunk> for C` impls
    /// where `C` is one of the concrete chunk types.
    pub fn verify_min_length(&self, ty: u8, len: u16) -> Result<(), GenericTryFromChunkError> {
        if self.ty() != ty {
            Err(GenericTryFromChunkError::InvalidType)?;
        }

        if self.length() < len {
            Err(GenericTryFromChunkError::InvalidLength)?;
        }

        Ok(())
    }

    /// Helper function to verify that this chunk has the specified type and
    /// exact length. Intended for usage in `TryFrom<Chunk> for C` impls where
    /// `C` is one of the concrete chunk types.
    pub fn verify_static_length(&self, ty: u8, len: u16) -> Result<(), GenericTryFromChunkError> {
        if self.ty() != ty {
            Err(GenericTryFromChunkError::InvalidType)?;
        }

        if self.length() != len {
            Err(GenericTryFromChunkError::InvalidLength)?;
        }

        Ok(())
    }

    /// Parse chunk parameters beginning from byte `from_byte` in `self.value()`.
    /// This is typically byte 0 but chunks with fixed parameters require adjustment.
    ///
    /// For example the `INIT` chunk has 5 fixed parameters with a length of 16 bytes, which means
    /// a call of this method would look like `chunk.parameters(16)`.
    ///
    /// The method processes parameters according to [RFC 4960 § 3.2.1] and will stop or skip in
    /// case an unrecognized parameter is found.
    ///
    /// [RFC 4960 § 3.2.1]: https://tools.ietf.org/html/rfc4960#section-3.2.1
    pub fn parameters(&self, from_byte: usize) -> Result<ParsedTLV<'_>, TryTLVFromBytesError> {
        #[allow(clippy::indexing_slicing)]
        TLV::from_list(&self.value()[from_byte..], super::generic_tlv::parameter::is_recognized)
    }
}

impl<'bytes> TryFrom<&'bytes [u8]> for Chunk<'bytes> {
    type Error = BytesToChunkError;

    #[allow(clippy::shadow_reuse)]
    fn try_from(bytes: &'bytes [u8]) -> Result<Self, Self::Error> {
        if bytes.len() < HEADER_SIZE as usize {
            return Err(BytesToChunkError::TooShort);
        }

        #[allow(clippy::indexing_slicing)]
        let length = NetworkEndian::read_u16(&bytes[2..=3]);

        // because the length includes the header it has to be at least that size
        if length < HEADER_SIZE {
            return Err(BytesToChunkError::InvalidLength);
        }

        // the given slice does not contain enough bytes to correctly construct a chunk
        // with the given length
        if bytes.len() < length as usize {
            return Err(BytesToChunkError::InvalidLength);
        }

        // we only save a reference to the part that interests us
        #[allow(clippy::indexing_slicing)]
        let bytes = &bytes[..length as usize];

        Ok(Chunk { bytes })
    }
}

/// Error when parsing `Chunk` from bytes.
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
#[allow(variant_size_differences)]
pub enum BytesToChunkError {
    /// Chunk is too short to be a valid chunk.
    TooShort,
    /// Chunk has a invalid length, indicating a [ProtocolViolation].
    ///
    /// [ProtocolViolation]: ../generic_tlv/error/protocol_violation/struct.ProtocolViolation.html
    InvalidLength,
}

/// Error when trying to convert a general chunk into a spezialized one.
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum GenericTryFromChunkError {
    /// The conversion from a generic chunk to a specialized one failed because the type byte does
    /// not match the required value.
    InvalidType,
    /// Chunk has a invalid length, indicating a [ProtocolViolation].
    ///
    /// [ProtocolViolation]: ../generic_tlv/error/protocol_violation/struct.ProtocolViolation.html
    InvalidLength,
}

#[cfg(test)]
mod tests {
    use std::convert::TryFrom;

    use super::{BytesToChunkError, Chunk};

    #[test]
    fn parse_chunk() {
        let bytes = [14, 0, 0, 4];

        let expected_chunk = Chunk { bytes: &[14, 0, 0, 4] };

        assert_eq!(Ok(expected_chunk), Chunk::try_from(&bytes[..]));
    }

    #[test]
    fn parse_chunk_with_below_minimum_bytes() {
        let bytes = [2, 0, 0];

        assert_eq!(Err(BytesToChunkError::TooShort), Chunk::try_from(&bytes[..]));
    }

    #[test]
    fn parse_chunk_with_bad_length() {
        let bytes = [7, 0, 0, 8, 13, 37];

        assert_eq!(Err(BytesToChunkError::InvalidLength), Chunk::try_from(&bytes[..]));
    }

    #[test]
    fn parse_chunk_with_two_chunks_as_bytes() {
        let bytes = [10, 0, 0, 5, 3, 14, 0, 0, 4];

        let expected_chunk = Chunk {
            bytes: &[10, 0, 0, 5, 3],
        };

        #[allow(clippy::result_unwrap_used)]
        let resulting_chunk = Chunk::try_from(&bytes[..]).unwrap();

        assert_eq!(expected_chunk, resulting_chunk);
        assert_eq!(5, resulting_chunk.length());
    }
}
