/// This module contains everything regarding configuration and setup of RSCTP endpoints.
pub mod config;
/// Endpoints that can receive and send data.
pub mod endpoint;
