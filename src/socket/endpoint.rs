use etherparse::IpHeader;
use etherparse::IpTrafficClass;
use etherparse::PacketHeaders;
use pnet::datalink::{channel, Channel, ChannelType, Config, DataLinkReceiver, /*DataLinkSender, */ NetworkInterface,};

use crate::error::{Error, Result};
use crate::network::MacAddress;

use super::config::Configuration;

/// A basic SCTP Endpoint holding a sender and receiver and its state data.
pub struct Endpoint {
    /// NetworkInterface
    interface: NetworkInterface,
    //    tx: Box<dyn DataLinkSender>,
    /// DataLinkReceiver
    rx: Box<dyn DataLinkReceiver>,
}

impl Endpoint {
    /// Create a new socket with a given interface (see `../network`) and a configuration.
    pub fn create(interface: NetworkInterface, config: &Configuration) -> Result<Self> {
        let config = Config {
            read_buffer_size: config.read_buffer.checked_mul(1522).ok_or(Error::Overflow)?,
            write_buffer_size: config.write_buffer.checked_mul(1522).ok_or(Error::Overflow)?,
            read_timeout: config.read_timeout,
            write_timeout: config.write_timeout,
            channel_type: ChannelType::Layer2,
            bpf_fd_attempts: 1000,
            linux_fanout: None,
        };

        match channel(&interface, config)? {
            Channel::Ethernet(_tx, rx) => {
                Ok(Self {
                    interface,
                    /*tx,*/ rx,
                })
            }
            Channel::PleaseIncludeACatchAllVariantWhenMatchingOnThisEnum => unreachable!(),
        }
    }

    #[cfg(test)]
    fn create_dummy(i: u8, config: &Configuration) -> Result<Self> {
        let interface = pnet::datalink::dummy::dummy_interface(i);
        Self::create(interface, config)
    }

    /// receive
    pub fn receive_once(&mut self) -> Result<(IpHeader, Vec<u8>)> {
        let bytes = self.rx.next().map_err(Error::from)?;
        let packet = PacketHeaders::from_ethernet_slice(bytes).map_err(Error::from)?;

        let link = packet.link;
        let _vlan = packet.vlan;
        let ip = packet.ip;
        let _transport = packet.transport;
        let payload = packet.payload;

        match link {
            None => return Err(Error::NoLink),
            Some(eth_header) => {
                let directed_at_us = if self.interface.is_loopback() {
                    true
                } else {
                    MacAddress::from(eth_header.destination) == self.interface.mac_address().into()
                };

                if !directed_at_us {
                    return Err(Error::BadDestination);
                }
            }
        }

        // we currently do not care about vlan
        //        match packet.vlan {
        //            None => {},
        //            Some(_) => {},
        //        }

        match ip {
            None => Err(Error::NotIP),
            Some(ip) => {
                let traffic_class = match &ip {
                    IpHeader::Version4(header) => header.protocol,
                    IpHeader::Version6(header) => header.next_header,
                };

                if traffic_class == IpTrafficClass::Sctp as u8 {
                    Ok((ip, payload.to_owned()))
                } else {
                    Err(Error::NotInteresting)
                }
                /*else if traffic_class == IpTrafficClass::Udp as u8 {
                    Ok((ip, payload.to_owned())) // TODO: cast contents for SCTP over UDP
                } */
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::socket::config::Configuration;

    #[test]
    fn create_dummy_interface() {
        let config = Configuration::default();
        match super::Endpoint::create_dummy(0, &config) {
            Ok(_endpoint) => {}
            Err(_e) => {}
        }
    }
}
