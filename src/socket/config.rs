use std::time::Duration;

use pnet::datalink::Config;

use crate::error::{Error, Result};
use crate::utilities::ETHERNET_MAX_PACKET_SIZE;

/// The configuration used when creating a new RSCTP Endpoint.
#[derive(Clone, Copy, Debug)]
pub struct Configuration {
    /// Buffer `n` maximum size ethernet packets when reading.
    /// This corresponds to `n * ETHERNET_MAX_PACKET_SIZE` bytes in memory.
    /// Defaults to 3.
    pub read_buffer: usize,
    /// Buffer `n` maximum size ethernet packets when writing.
    /// This corresponds to `n * ETHERNET_MAX_PACKET_SIZE` bytes in memory.
    /// Defaults to 3.
    pub write_buffer: usize,
    /// Linux/Berkeley Packet Filter/Netmap only:
    /// The read timeout. Defaults to None.
    pub read_timeout: Option<Duration>,
    /// Linux/Berkeley Packet Filter/Netmap only:
    /// The write timeout. Defaults to None.
    pub write_timeout: Option<Duration>,
    /// Berkeley Packet Filter/OS X only:
    /// The number of /dev/bpf* file descriptors to attempt before failing.
    pub bpf_fd_attempts: usize,
}

impl Configuration {
    /// convenience function
    pub fn with_read_buffer_size_in_packets(self, n: usize) -> Result<Self> {
        Ok(Self {
            read_buffer: ETHERNET_MAX_PACKET_SIZE.checked_mul(n).ok_or(Error::Overflow)?,
            ..self
        })
    }

    /// convenience function
    pub fn with_write_buffer_size_in_packets(self, n: usize) -> Result<Self> {
        Ok(Self {
            write_buffer: ETHERNET_MAX_PACKET_SIZE.checked_mul(n).ok_or(Error::Overflow)?,
            ..self
        })
    }
}

impl Default for Configuration {
    fn default() -> Self {
        let pnet_default = Config::default();
        Self {
            read_buffer: 3,
            write_buffer: 3,
            read_timeout: pnet_default.read_timeout,
            write_timeout: pnet_default.write_timeout,
            bpf_fd_attempts: pnet_default.bpf_fd_attempts,
        }
    }
}
