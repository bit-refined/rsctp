use std::convert::TryFrom;

use honggfuzz::fuzz;

use rsctp::network::chunk::{
    abort::TYPE_ID as TYPE_ID_ABORT, cookie_ack::TYPE_ID as TYPE_ID_COOKIE_ACK,
    cookie_echo::TYPE_ID as TYPE_ID_COOKIE_ECHO, data::TYPE_ID as TYPE_ID_DATA, error::TYPE_ID as TYPE_ID_ERROR,
    heartbeat::TYPE_ID as TYPE_ID_HEARTBEAT, heartbeat_ack::TYPE_ID as TYPE_ID_HEARTBEAT_ACK,
    init::TYPE_ID as TYPE_ID_INIT, init_ack::TYPE_ID as TYPE_ID_INIT_ACK, sack::TYPE_ID as TYPE_ID_SACK,
    shutdown::TYPE_ID as TYPE_ID_SHUTDOWN, shutdown_ack::TYPE_ID as TYPE_ID_SHUTDOWN_ACK,
    shutdown_complete::TYPE_ID as TYPE_ID_SHUTDOWN_COMPLETE, Abort, CookieACK, CookieEcho, Data, Error, Heartbeat,
    HeartbeatACK, Init, InitACK, Shutdown, ShutdownACK, ShutdownComplete, SACK,
};
use rsctp::network::packet::Packet;

fn main() {
    loop {
        fuzz!(|data: &[u8]| {
            let _ = Packet::try_from(&data[..]).map(|p| {
                for chunk in p.into_chunks() {
                    assert!(chunk.length() >= 4);

                    match chunk.ty() {
                        TYPE_ID_ABORT => {
                            let _ = Abort::try_from(chunk);
                        }
                        TYPE_ID_COOKIE_ACK => {
                            let _ = CookieACK::try_from(chunk);
                        }
                        TYPE_ID_COOKIE_ECHO => {
                            let _ = CookieEcho::try_from(chunk);
                        }
                        TYPE_ID_DATA => {
                            let _ = Data::try_from(chunk);
                        }
                        TYPE_ID_ERROR => {
                            let _ = Error::try_from(chunk);
                        }
                        TYPE_ID_HEARTBEAT => {
                            let _ = Heartbeat::try_from(chunk);
                        }
                        TYPE_ID_HEARTBEAT_ACK => {
                            let _ = HeartbeatACK::try_from(chunk);
                        }
                        TYPE_ID_INIT => {
                            let _ = Init::try_from(chunk);
                        }
                        TYPE_ID_INIT_ACK => {
                            let _ = InitACK::try_from(chunk);
                        }
                        TYPE_ID_SACK => {
                            let _ = SACK::try_from(chunk);
                        }
                        TYPE_ID_SHUTDOWN => {
                            let _ = Shutdown::try_from(chunk);
                        }
                        TYPE_ID_SHUTDOWN_ACK => {
                            let _ = ShutdownACK::try_from(chunk);
                        }
                        TYPE_ID_SHUTDOWN_COMPLETE => {
                            let _ = ShutdownComplete::try_from(chunk);
                        }
                        _ => (),
                    };
                }
            });
        });
    }
}
