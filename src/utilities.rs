/// Size in bytes of an Ethernet II frame.
pub const ETHERNET_MAX_PACKET_SIZE: usize = 1522;

/// Minimum MTU required of all links supporting IPv4. See [RFC 791 § 3.1].
///
/// [RFC 791 § 3.1]: https://tools.ietf.org/html/rfc791#section-3.1
pub const IP_MIN_MTU_V4: usize = 576;

/// Minimum MTU required of all links supporting IPv6. See [RFC 8200 § 5].
///
/// [RFC 8200 § 5]: https://tools.ietf.org/html/rfc8200#section-5
pub const IP_MIN_MTU_V6: usize = 1280;
