use eui48::MacAddress as EuiMac;
use pnet::datalink::NetworkInterface;
use pnet::util::MacAddr as PnetMac;
use pnet::util::MacAddr;

/// Chunk types.
pub mod chunk;
/// Parameters and errors.
pub mod generic_tlv;
/// SCTP Packet.
pub mod packet;
/// Functions for padding in SCTP.
pub mod padding;

/// A media access control address compatible with the [EUI48 MAC](eui48::MacAddress) and  [libpnet MAC](pnet::util::MacAddr).
#[derive(Clone, Copy, Debug, PartialEq)]
pub struct MacAddress(pub [u8; 6]);

impl From<EuiMac> for MacAddress {
    fn from(mac: EuiMac) -> Self {
        Self(mac.to_array())
    }
}

impl From<&EuiMac> for MacAddress {
    fn from(mac: &EuiMac) -> Self {
        Self(mac.to_array())
    }
}

impl From<PnetMac> for MacAddress {
    fn from(mac: PnetMac) -> Self {
        let PnetMac(byte_1, byte_2, byte_3, byte_4, byte_5, byte_6) = mac;
        Self([byte_1, byte_2, byte_3, byte_4, byte_5, byte_6])
    }
}

impl From<&PnetMac> for MacAddress {
    fn from(mac: &PnetMac) -> Self {
        let PnetMac(byte_1, byte_2, byte_3, byte_4, byte_5, byte_6) = mac;
        Self([*byte_1, *byte_2, *byte_3, *byte_4, *byte_5, *byte_6])
    }
}

impl From<MacAddress> for PnetMac {
    fn from(mac: MacAddress) -> Self {
        Self(mac.0[0], mac.0[1], mac.0[2], mac.0[3], mac.0[4], mac.0[5])
    }
}

//impl From<&[u8]> for MacAddress {
//    fn from(slice: &[u8]) -> Self {
//        Self([slice[0], slice[1], slice[2], slice[3], slice[4], slice[5]])
//    }
//}

impl From<&[u8; 6]> for MacAddress {
    fn from(array: &[u8; 6]) -> Self {
        Self(*array)
    }
}

impl From<[u8; 6]> for MacAddress {
    fn from(array: [u8; 6]) -> Self {
        Self(array)
    }
}

/// Get a list of available network interfaces.
// this function exists in case we ever support more libraries as backends or our OS-specific code
pub fn get_interfaces() -> Vec<NetworkInterface> {
    pnet::datalink::interfaces()
}

/// Get a list of available network interfaces containing a given name.
pub fn get_interfaces_containing(partial_name: &str) -> Vec<NetworkInterface> {
    let mut interfaces = get_interfaces();
    interfaces.retain(|interface| interface.name.contains(partial_name));
    interfaces
}

/// Get a network interface with the given name.
pub fn get_interface_by_name(name: &str) -> Option<NetworkInterface> {
    for interface in get_interfaces() {
        if interface.name == name {
            return Some(interface);
        }
    }

    None
}

/// Get a network interface with the given [MAC Address](struct.MacAddress.html).
pub fn get_interface_by_mac(mac: MacAddress) -> Option<NetworkInterface> {
    let mac: MacAddr = mac.into();
    for interface in get_interfaces() {
        // .mac_address() is interface.mac.unwrap(), which we can easily prevent like this
        if let Some(i_mac) = interface.mac {
            if i_mac == mac {
                return Some(interface);
            }
        }
    }

    None
}

#[cfg(test)]
mod tests {
    use eui48::MacAddress as EuiMac;
    use pnet::util::MacAddr as PnetMac;

    use super::MacAddress;

    #[test]
    fn convert_pnet_mac() {
        let mac = PnetMac::new(1, 2, 3, 4, 5, 6);
        assert_eq!(MacAddress([1, 2, 3, 4, 5, 6]), mac.into())
    }

    #[test]
    fn convert_eui_mac() {
        let mac = EuiMac::new([1, 2, 3, 4, 5, 6]);
        assert_eq!(MacAddress([1, 2, 3, 4, 5, 6]), mac.into())
    }
}
