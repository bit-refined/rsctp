//! Rust Stream Control Transmission Protocol

// These are outright wrong and are forbidden to be allowed. This forces correct code.
// If there is indeed a false positive in one of these, they can still be moved to the deny section.
#![forbid(
    const_err,
    exceeding_bitshifts,
    future_incompatible,
    irrefutable_let_patterns,
    macro_use_extern_crate,
    mutable_transmutes,
    no_mangle_const_items,
    nonstandard_style,
    rust_2018_compatibility,
    rust_2018_idioms,
    trivial_casts,
    trivial_numeric_casts,
    unknown_crate_types,
    unreachable_pub,
    unsafe_code,
    unused,
    unused_import_braces,
    unused_lifetimes,
    unused_results,
    deprecated,
    illegal_floating_point_literal_pattern,
    improper_ctypes,
    intra_doc_link_resolution_failure,
    irrefutable_let_patterns,
    late_bound_lifetime_arguments,
    non_camel_case_types,
    non_shorthand_field_patterns,
    non_snake_case,
    non_upper_case_globals,
    no_mangle_generic_items,
    overflowing_literals,
    path_statements,
    patterns_in_fns_without_body,
    plugin_as_library,
    private_in_public,
    proc_macro_derive_resolution_fallback,
    renamed_and_removed_lints,
    safe_packed_borrows,
    stable_features,
    trivial_bounds,
    type_alias_bounds,
    tyvar_behind_raw_pointer,
    unconditional_recursion,
    unions_with_drop_fields,
    unknown_lints,
    unnameable_test_items,
    unreachable_patterns,
    unstable_name_collisions,
    where_clauses_object_safety,
    while_true,
    clippy::complexity,
    clippy::correctness,
    clippy::perf,
    clippy::style
)]
// These are denied so one is forced to annotate expected behaviour by allowing it.
#![deny(
    dead_code,
    missing_copy_implementations,
    missing_doc_code_examples,
    unconditional_recursion,
    unreachable_code,
    unused_qualifications,
    variant_size_differences,
    clippy::cast_possible_truncation,
    clippy::cast_possible_wrap,
    clippy::cast_precision_loss,
    clippy::cast_sign_loss,
    clippy::decimal_literal_representation,
    clippy::empty_line_after_outer_attr,
    clippy::eval_order_dependence,
    clippy::indexing_slicing,
    clippy::mutex_integer,
    clippy::needless_pass_by_value,
    clippy::non_ascii_literal,
    clippy::nursery,
    clippy::option_unwrap_used,
    clippy::pedantic,
    clippy::restriction,
    clippy::result_unwrap_used
)]
// These can either not be #[allow()]'d, have false-positives or are development-tools
// of which we only care about in the CI.
#![warn(
    missing_docs,
    clippy::missing_docs_in_private_items,
    clippy::float_arithmetic,
    clippy::inline_always,
    clippy::integer_arithmetic,
    clippy::missing_inline_in_public_items,
    clippy::multiple_crate_versions,
    clippy::shadow_reuse,
    clippy::shadow_same,
    clippy::unimplemented,
    clippy::use_self
)]
#![allow(
    box_pointers,
    missing_debug_implementations,
    clippy::cargo,
    clippy::implicit_return,
    clippy::missing_inline_in_public_items
)]
// Set this to `warn` again if https://github.com/rust-lang/rust/issues/54079 is resolved.
#![allow(single_use_lifetimes)]
// allow unwraps in tests
#![cfg_attr(test, allow(clippy::option_unwrap_used, clippy::result_unwrap_used))]

/// Error stuff.
pub mod error;
/// Networking stuff.
pub mod network;
/// Socket stuff.
pub mod socket;
/// Utilities
pub mod utilities;

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2_u8.saturating_add(2), 4);
    }
}
